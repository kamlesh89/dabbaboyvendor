package shree.techno.dabbaboyvendor.deliveryboy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.deliveryboy.DeliveryMainActivity
import shree.techno.dabbaboyvendor.deliveryboy.pojo.DeliveryOrderDetails
import shree.techno.dabbaboyvendor.util.SessionManager

class DeliveryMyFreeOrderAdapter(
    var ctx: Context,
    var list: ArrayList<DeliveryOrderDetails>
) :
    RecyclerView.Adapter<DeliveryMyFreeOrderAdapter.MyHolder>() {

    var sessionManager: SessionManager? = null

    init {
        sessionManager = SessionManager(ctx)
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tiffintext: TextView? = null
        var tiffincode: TextView? = null
        var pick_name: TextView? = null
        var pick_mobile: TextView? = null
        var pick_address: TextView? = null
        var del_name: TextView? = null
        var del_mobile: TextView? = null
        var del_address: TextView? = null
        var submit_bt: Button? = null

        init {
            tiffintext = itemView.findViewById(R.id.tv_delivadpmyorder_tiifin)
            tiffincode = itemView.findViewById(R.id.tv_delivadpmyorder_code)
            pick_name = itemView.findViewById(R.id.tv_delivadpmyorder_pickname)
            pick_mobile = itemView.findViewById(R.id.tv_delivadpmyorder_pickmobile)
            pick_address = itemView.findViewById(R.id.tv_delivadpmyorder_pickaddress)
            del_name = itemView.findViewById(R.id.tv_delivadpmyorder_delname)
            del_mobile = itemView.findViewById(R.id.tv_delivadpmyorder_delmobile)
            del_address = itemView.findViewById(R.id.tv_delivadpmyorder_deladdress)
            submit_bt = itemView.findViewById(R.id.bt_delivadpmyorder_submit)
        }

        fun bindView(myOrder: DeliveryOrderDetails) {

            tiffintext?.text = "Status - "
            tiffincode?.text = getStatustext(myOrder.status)
            pick_name?.text = myOrder.vendorDetail.getString("name")
            pick_mobile?.text = myOrder.vendorDetail.getString("mobile_no")
            pick_address?.text = myOrder.vendorDetail.getString("address")
            del_name?.text = myOrder.username
            del_mobile?.text = myOrder.usermobile
            del_address?.text = Utility.getAddress(myOrder.addressDetail)
            submit_bt?.text = getStatusButtontext(myOrder.status)
            if (getStatusButtontext(myOrder.status).equals("")) {
                submit_bt?.visibility = View.GONE
            }
        }

        private fun getStatusButtontext(status: String): String {
            if (status.equals("2")) {
                return "Pick up"
            } else if (status.equals("3")) {
                return "Delivered"
            } else {
                return ""
            }
        }

        private fun getStatustext(status: String): String {
            if (status.equals("2")) {
                return "Alloted To Delivery Boy"
            } else if (status.equals("3")) {
                return "Picked By Delivery Boy"
            } else {
                return "Delivered To User"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.deliveryadp_myorder_view, parent, false)
        return MyHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        var data = list[position]
        holder.bindView(data)
        holder.submit_bt?.setOnClickListener(View.OnClickListener {
            var status = list[position].status
            if (status.equals("2") || status.equals("3")) {
                callApi(getBody(status, data))
            }
        })
    }

    private fun getBody(
        status: String,
        data: DeliveryOrderDetails
    ): JSONObject {

        var deliv_id = sessionManager?.getData(SessionManager.KEY_ID)
        if (status.equals("2")) {
            var body =
                ApiObjects.getupdateFreeDabbaStatus(data.id, deliv_id, "3")
            return body
        } else if (status.equals("3")) {
            var body =
                ApiObjects.getupdateFreeDabbaStatus(data.id, deliv_id, "4")
            return body
        } else if (status.equals("4")) {
            var body =
                ApiObjects.getupdateFreeDabbaStatus(data.id, deliv_id, "5")
            return body
        } else if (status.equals("5")) {
            var body =
                ApiObjects.getupdateFreeDabbaStatus(data.id, deliv_id, "6")
            return body
        } else {
            return JSONObject()
        }
    }

    // update call api
    private fun callApi(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_FREE_DABBA_STATUS)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            (ctx as DeliveryMainActivity).setOrder()
        } else {
            Utility.toastView(message, ctx)
        }
    }

}