package shree.techno.dabbaboyvendor.deliveryboy.pojo

import org.json.JSONObject

data class DeliveryOrderDetails(
    var id: String,
    var tiffincode: String,
    var status: String,
    var username: String,
    var usermobile: String,
    var dateorder_id: String,
    var type: String,
    var addressDetail: JSONObject,
    var vendorDetail: JSONObject,
    var latitude: Double,
    var longitude: Double,
    var distance: Double
) : Comparable<DeliveryOrderDetails> {

    override fun compareTo(other: DeliveryOrderDetails): Int {
        return this.distance.compareTo(other.distance)
    }
}