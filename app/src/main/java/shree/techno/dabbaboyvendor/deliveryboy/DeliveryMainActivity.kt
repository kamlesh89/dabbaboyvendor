package shree.techno.dabbaboyvendor.deliveryboy

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Constant
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_delivery_main.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.LocationTrack
import shree.techno.dabbaboyvendor.util.SessionManager


class DeliveryMainActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        iv_delmain_logout.setOnClickListener(this)
        tv_delmain_myorder.setOnClickListener(this)
        tv_delmain_myprofile.setOnClickListener(this)
        requestPermission()
    }

    // location permission
    fun requestPermission(): Unit {
        Dexter.withActivity(this).withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        setOrder()
                    } else {
                        finish()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Utility.toastView("Your Location required", ctx)
                finish()
            }
            .onSameThread()
            .check()
    }


    fun setOrder() {
        Utility.setDeliveryFragment(
            DeliveryMyOrderFragment(ctx, Constant.DELIVERY_TITLE_MY_ORDER)
            , Constant.DELIVERY_TITLE_MY_ORDER, this
        )
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_delivery_main
    }

    override fun onClick(p0: View?) {

        when (p0?.id) {
            R.id.iv_delmain_logout -> {
                userLogout()
            }
            R.id.tv_delmain_myorder -> {
                setOrder()
            }
            R.id.tv_delmain_myprofile -> {
                if (ConnectionDetector.isConnected()) {
                    updateLocation()
                }
                Utility.setDeliveryFragment(
                    DeliveryMyProfileFragment(ctx, Constant.DELIVERY_TITLE_MY_PROFILE)
                    , Constant.DELIVERY_TITLE_MY_PROFILE, this
                )
            }
        }
    }

    private fun userLogout() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(ctx.getString(R.string.logout_msg))
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
                SessionManager(ctx).logoutUser(this)
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    fun setHeadTitle(title: String) {
        tv_delmain_title.setText(title)
        if (title.equals(Constant.DELIVERY_TITLE_MY_ORDER)) {
            tv_delmain_myorder.setTypeface(null, Typeface.BOLD);
            tv_delmain_myprofile.setTypeface(null, Typeface.NORMAL);
        } else {
            tv_delmain_myorder.setTypeface(null, Typeface.NORMAL);
            tv_delmain_myprofile.setTypeface(null, Typeface.BOLD);
        }
    }

    fun updateLocation() {
        var locationTrack = LocationTrack(ctx)
        var lati = locationTrack.latitude
        var longi = locationTrack.longitude

        var api_body = ApiObjects.updateboyLocation(
            sessionManager.getData(SessionManager.KEY_ID),
            lati, longi
        )
        AndroidNetworking.post(WebApis.API_DELOVEY_BOYS_UPDATE_LOCATION)
            .addJSONObjectBody(api_body)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

}
