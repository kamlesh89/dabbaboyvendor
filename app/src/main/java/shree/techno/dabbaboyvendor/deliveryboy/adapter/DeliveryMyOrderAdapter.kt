package shree.techno.dabbaboyvendor.deliveryboy.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.deliveryboy.DeliveryMainActivity
import shree.techno.dabbaboyvendor.deliveryboy.pojo.DeliveryOrderDetails


class DeliveryMyOrderAdapter(
    var ctx: Context,
    var list: List<DeliveryOrderDetails>
) :
    RecyclerView.Adapter<DeliveryMyOrderAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tiffin_type: TextView? = null
        var tiffincode: TextView? = null
        var pick_name: TextView? = null
        var pick_mobile: TextView? = null
        var pick_address: TextView? = null
        var del_name: TextView? = null
        var del_mobile: TextView? = null
        var del_address: TextView? = null
        var distance: TextView? = null
        var submit_bt: Button? = null
        var direction_iv: ImageView? = null

        init {
            distance = itemView.findViewById(R.id.tv_delivadpmyorder_distance)
            tiffin_type = itemView.findViewById(R.id.tv_delivadpmyorder_tiifin)
            tiffincode = itemView.findViewById(R.id.tv_delivadpmyorder_code)
            pick_name = itemView.findViewById(R.id.tv_delivadpmyorder_pickname)
            pick_mobile = itemView.findViewById(R.id.tv_delivadpmyorder_pickmobile)
            pick_address = itemView.findViewById(R.id.tv_delivadpmyorder_pickaddress)
            del_name = itemView.findViewById(R.id.tv_delivadpmyorder_delname)
            del_mobile = itemView.findViewById(R.id.tv_delivadpmyorder_delmobile)
            del_address = itemView.findViewById(R.id.tv_delivadpmyorder_deladdress)
            submit_bt = itemView.findViewById(R.id.bt_delivadpmyorder_submit)
            direction_iv = itemView.findViewById(R.id.iv_delivadpmyorder_direction)
        }

        fun bindView(myOrder: DeliveryOrderDetails) {

            distance!!.text = "Distance - ${myOrder.distance}"
            tiffincode?.text = myOrder.tiffincode
            pick_name?.text = myOrder.vendorDetail.getString("name")
            pick_mobile?.text = myOrder.vendorDetail.getString("mobile_no")
            pick_address?.text = myOrder.vendorDetail.getString("address")
            del_name?.text = myOrder.username
            del_mobile?.text = myOrder.usermobile
            del_address?.text = Utility.getAddress(myOrder.addressDetail)
            submit_bt?.text = getStatusButtontext(myOrder.status)
            if (myOrder.status.equals("3")) {
                direction_iv?.visibility = View.VISIBLE
            } else {
                direction_iv?.visibility = View.GONE
            }
            if (getStatusButtontext(myOrder.status).equals("")) {
                submit_bt?.visibility = View.GONE
            } else if (myOrder.status.equals("4") && myOrder.tiffincode.contains("DIS")) {
                submit_bt?.visibility = View.GONE
            } else if (myOrder.status.equals("5") && myOrder.tiffincode.contains("DIS")) {
                submit_bt?.visibility = View.GONE
            }

            if (myOrder.tiffincode.contains("DIS")) {
                tiffin_type?.setText("Disposal Code - ")
            } else {
                tiffin_type?.setText("Hot Dabba Code - ")
            }

        }

        private fun getStatusButtontext(status: String): String {
            if (status.equals("2")) {
                return "Pick up"
            } else if (status.equals("3")) {
                return "Delivered"
            } else if (status.equals("4")) {
                return "Return Pick"
            } else if (status.equals("5")) {
                return "Return Drop"
            } else {
                return ""
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.deliveryadp_myorder_view, parent, false)
        return MyHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        var data = list[position]
        holder.bindView(data)
        holder.submit_bt?.setOnClickListener(View.OnClickListener {
            var status = list[position].status
            if (status.equals("2") || status.equals("3") || status.equals("4") || status.equals("5")) {
                callApi(getBody(status, data))
            }
        })
        holder.direction_iv?.setOnClickListener(View.OnClickListener {
            var lat = data.addressDetail.getString("latitude")
            var longi = data.addressDetail.getString("longitude")
            val url =
                "http://maps.google.com/maps?daddr=" + lat + "," + longi
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            (ctx as DeliveryMainActivity).startActivity(intent)
        })
    }

    private fun getBody(
        status: String,
        data: DeliveryOrderDetails
    ): JSONObject {
        if (status.equals("2")) {
            var body =
                ApiObjects.upadteDeliveryStatus(data.dateorder_id, data.type, "3", data.tiffincode)
            return body
        } else if (status.equals("3")) {
            var body =
                ApiObjects.upadteDeliveryStatus(data.dateorder_id, data.type, "4", data.tiffincode)
            return body
        } else if (status.equals("4")) {
            var body =
                ApiObjects.upadteDeliveryStatus(data.dateorder_id, data.type, "5", data.tiffincode)
            return body
        } else if (status.equals("5")) {
            var body =
                ApiObjects.upadteDeliveryStatus(data.dateorder_id, data.type, "6", data.tiffincode)
            return body
        } else {
            return JSONObject()
        }
    }

    // update call api
    private fun callApi(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_STATUS)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            (ctx as DeliveryMainActivity).setOrder()
        } else {
            Utility.toastView(message, ctx)
        }
    }

}