package shree.techno.dabbaboyvendor.deliveryboy

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class DeliveryMyProfileFragment(var ctx: Context, var title: String) : Fragment() {

    var sessionManager: SessionManager? = null

    init {
        (ctx as DeliveryMainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
        if (ConnectionDetector.isConnected()) {
            (ctx as DeliveryMainActivity).updateLocation()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.deliveryfragment_myprofile, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View) {

        var name_tv = myview.findViewById<TextView>(R.id.tv_delprofile_name)
        var mobile_tv = myview.findViewById<TextView>(R.id.tv_delprofile_mobile)
        var email_tv = myview.findViewById<TextView>(R.id.tv_delprofile_email)
        var aadhar_tv = myview.findViewById<TextView>(R.id.tv_delprofile_aadhar)

        name_tv.setText(sessionManager?.getData(SessionManager.KEY_NAME))
        mobile_tv.setText(sessionManager?.getData(SessionManager.KEY_MOBILE))
        email_tv.setText(sessionManager?.getData(SessionManager.KEY_EMAIL_ID))
        aadhar_tv.setText(sessionManager?.getData(SessionManager.KEY_AREA))
    }

}
