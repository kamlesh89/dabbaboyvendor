package shree.techno.dabbaboyvendor.deliveryboy

import android.Manifest
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.deliveryboy.adapter.DeliveryMyFreeOrderAdapter
import shree.techno.dabbaboyvendor.deliveryboy.adapter.DeliveryMyOrderAdapter
import shree.techno.dabbaboyvendor.deliveryboy.pojo.DeliveryOrderDetails
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.LocationTrack
import shree.techno.dabbaboyvendor.util.SessionManager
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class DeliveryMyOrderFragment(var ctx: Context, var title: String) : Fragment(),
    View.OnClickListener {

    init {
        if (ConnectionDetector.isConnected()) {
            (ctx as DeliveryMainActivity).updateLocation()
        }
    }

    var recyclerView: RecyclerView? = null
    var pending_tv: TextView? = null
    var done_tv: TextView? = null
    var free_tv: TextView? = null
    var coordinatorLayout: CoordinatorLayout? = null
    var sessionManager: SessionManager? = null

    var list: ArrayList<DeliveryOrderDetails>? = null
    var free_list: ArrayList<DeliveryOrderDetails>? = null

    val TYPE_PENDING = "pending"
    val TYPE_DELIVERED = "delivered"
    val TYPE_FREE = "free"

    init {
        (ctx as DeliveryMainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.deliveryfragment_myorders, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View) {

        recyclerView = myview.findViewById(R.id.rv_delmyorder_recyclerview)
        pending_tv = myview.findViewById(R.id.tv_delmyorder_pending)
        done_tv = myview.findViewById(R.id.tv_delmyorder_done)
        free_tv = myview.findViewById(R.id.tv_delmyorder_free)
        coordinatorLayout = myview.findViewById(R.id.coordinator)

        pending_tv?.setOnClickListener(this)
        done_tv?.setOnClickListener(this)
        free_tv?.setOnClickListener(this)
        requestPermission()
    }

    // location permission
    fun requestPermission(): Unit {
        Dexter.withActivity(ctx as DeliveryMainActivity).withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        getData()
                    } else {
                        (ctx as DeliveryMainActivity).finish()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Utility.toastView("Error occurred! ", ctx)
            }
            .onSameThread()
            .check()
    }


    private fun getData() {
        if (ConnectionDetector.isConnected()) {
            var body =
                ApiObjects.getDeliverBoyOrder(sessionManager!!.getData(SessionManager.KEY_ID))
            callAPi(body)
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
        }
    }

    private fun callAPi(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_DEL_ORDERS)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var locationTrack = LocationTrack(ctx)

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            recyclerView?.visibility = View.VISIBLE
            var obj_data = response.getJSONObject("data")
            var subs_data = obj_data.getJSONArray("delivery_boy")
            var free_data = obj_data.getJSONArray("free_dabba")
            list = JsonParser.getDelOrderlist(
                subs_data,
                locationTrack.latitude,
                locationTrack.longitude
            )
            free_list = JsonParser.getDelfreeOrderlist(
                free_data,
                locationTrack.latitude,
                locationTrack.longitude
            )
            setIndicate(true, false, false)
            setAdapter(getFilterlist(list!!, TYPE_PENDING))
        } else {
            recyclerView?.visibility = View.GONE
            Utility.snacbarShow(message, coordinatorLayout!!)
        }
    }

    private fun getFilterlist(
        list: ArrayList<DeliveryOrderDetails>,
        type: String
    ): ArrayList<DeliveryOrderDetails> {
        var filterlist = ArrayList<DeliveryOrderDetails>()
        for (i in 0..list.size - 1) {
            var status = list[i].status
            if (type.equals(TYPE_PENDING)) {
                if (status.equals("2") || status.equals("3")) {
                    filterlist.add(list[i])
                }
            } else {
                if (status.equals("4") || status.equals("5") || status.equals("6")) {
                    filterlist.add(list[i])
                }
            }
        }
        return filterlist
    }

    private fun setAdapter(list: ArrayList<DeliveryOrderDetails>) {

        val sorted_list = list.sortedWith(compareBy<DeliveryOrderDetails> {
            it.distance
        })

        if (list.size > 0) {
            recyclerView?.visibility = View.VISIBLE
            recyclerView?.setHasFixedSize(true)
            recyclerView?.layoutManager = LinearLayoutManager(ctx)
            recyclerView?.adapter = DeliveryMyOrderAdapter(ctx, sorted_list)
        } else {
            recyclerView?.visibility = View.GONE
        }

        if (ConnectionDetector.isConnected()) {
            (ctx as DeliveryMainActivity).updateLocation()
        }
    }

    private fun setfreeAdapter(list: ArrayList<DeliveryOrderDetails>) {
        if (list.size > 0) {
            recyclerView?.visibility = View.VISIBLE
            recyclerView?.setHasFixedSize(true)
            recyclerView?.layoutManager = LinearLayoutManager(ctx)
            recyclerView?.adapter = DeliveryMyFreeOrderAdapter(ctx, list)
        } else {
            recyclerView?.visibility = View.GONE
        }
    }

    private fun setIndicate(pen: Boolean, del: Boolean, free: Boolean) {
        if (pen) {
            pending_tv?.setTypeface(null, Typeface.BOLD)
        } else {
            pending_tv?.setTypeface(null, Typeface.NORMAL)
        }

        if (del) {
            done_tv?.setTypeface(null, Typeface.BOLD)
        } else {
            done_tv?.setTypeface(null, Typeface.NORMAL)
        }

        if (free) {
            free_tv?.setTypeface(null, Typeface.BOLD)
        } else {
            free_tv?.setTypeface(null, Typeface.NORMAL)
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.tv_delmyorder_pending -> {
                setIndicate(true, false, false)
                setAdapter(getFilterlist(list!!, TYPE_PENDING))
            }
            R.id.tv_delmyorder_done -> {
                setIndicate(false, true, false)
                setAdapter(getFilterlist(list!!, TYPE_DELIVERED))
            }
            R.id.tv_delmyorder_free -> {
                setIndicate(false, false, true)
                setfreeAdapter(free_list!!)
            }
        }
    }


}
