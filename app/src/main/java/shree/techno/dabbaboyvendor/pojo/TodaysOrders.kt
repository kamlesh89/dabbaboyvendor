package shree.techno.dabbaboyvendor.pojo

import android.os.Parcelable
import org.json.JSONObject

data class TodaysOrders(
    var id: String,
    var order_id: String,
    var user_id: String,
    var date: String,
    var time: String,
    var qty: String,
    var vendor_id: String,
    var status: String,
    var address_id: String,
    var pack_name: String,
    var pack_id: String,
    var deliv_status: String,
    var tiffin_code: String,
    var address_object: JSONObject,
    var user_object: JSONObject,
    var tiffin_type: String
) {

}