package shree.techno.dabbaboyvendor.pojo

data class DeliveryBoy(
    val id: String,
    val name: String,
    val email: String,
    val mobile: String,
    val aadhar: String,
    val image: String
) {
}