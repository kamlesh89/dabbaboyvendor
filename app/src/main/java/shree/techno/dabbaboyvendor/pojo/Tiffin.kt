package shree.techno.dabbaboyvendor.pojo

data class Tiffin(
    var id: String,
    var vendor_id: String,
    var code: String,
    var status: String,
    var u_name: String,
    var u_mobile: String,
    var u_id: String,
    var d_name: String,
    var d_mobile: String,
    var d_id: String
) {
}