package shree.techno.dabbaboyvendor.pojo

data class MenuItem(
    var item_id: String,
    var item_name: String,
    var item_price: String,
    var item_qty: String,
    var pack_id: String,
    var header_id: String,
    var service_type: String,
    var status: String
) {
}