package shree.techno.dabbaboyvendor.pojo

import org.json.JSONArray

data class PackHeader(
    var id: String,
    var lunchdinner: String,
    var pack_id: String,
    var item_name: String,
    var variation: String,
    var date: String,
    var vendorItem: JSONArray
) {
}