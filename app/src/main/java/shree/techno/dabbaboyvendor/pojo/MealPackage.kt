package shree.techno.dabbaboyvendor.pojo

import org.json.JSONArray

data class MealPackage(
    val id: String,
    val name: String,
    val image: String,
    val verient: JSONArray,
    val adminpack : JSONArray
) {


}