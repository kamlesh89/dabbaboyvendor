package shree.techno.dabbaboyvendor.pojo

data class FreeOrder(
    var id: String,
    var user_id: String,
    var deliver_time: String,
    var type: String,
    var address_id: String,
    var status: String,
    var order_date: String,
    var vendor_id: String,
    var delivery_id: String
) {
}