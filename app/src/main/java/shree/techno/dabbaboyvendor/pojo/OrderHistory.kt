package shree.techno.dabbaboyvendor.pojo

data class OrderHistory(
    val id: String,
    val pack_name: String,
    val date: String,
    val tiffin_code: String,
    val tiffin_status: String,
    val deliv_status: String,
    val qty: String
) {
}