package shree.techno.dabbaboyvendor.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import shree.techno.dabbaboyvendor.R;
import shree.techno.dabbaboyvendor.pojo.DeliveryBoy;


public class MySpinnerAdapter extends ArrayAdapter<DeliveryBoy> {

    LayoutInflater flater;
    Context ctx;

    public MySpinnerAdapter(Activity context, int resouceId, int textviewId, ArrayList<DeliveryBoy> list) {

        super(context, resouceId, textviewId, list);
        ctx = context;
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView, position);
    }

    private View rowview(View convertView, int position) {

        DeliveryBoy rowItem = getItem(position);
        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.adp_spinnerview, null, false);

            holder.txtTitle = rowview.findViewById(R.id.tv_adpspinner_name);
            holder.txtMobile = rowview.findViewById(R.id.tv_adpspinner_mobile);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setTextColor(ContextCompat.getColor(ctx, R.color.colorBlack));
        holder.txtTitle.setText(rowItem.getName());
        holder.txtMobile.setText(rowItem.getMobile());
        return rowview;
    }

    private class viewHolder {
        TextView txtTitle;
        TextView txtMobile;
    }
}
