package shree.techno.dabbaboyvendor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.fragment.FragmentMyOrder
import shree.techno.dabbaboyvendor.pojo.Dashboard
import shree.techno.dabbaboyvendor.pojo.TodaysOrders

class DashboardAdapter(
    var ctx: Context,
    var list: ArrayList<Dashboard>,
    var orderList: ArrayList<TodaysOrders>
) :
    RecyclerView.Adapter<DashboardAdapter.MyHolder>() {

    class MyHolder(
        itemView: View,
        var orderList: ArrayList<TodaysOrders>
    ) : RecyclerView.ViewHolder(itemView) {

        val TYPE_LUNCH = "Lunch"
        val TYPE_DINNER = "Dinner"

        var pack_name_tv: TextView? = null
        var lunch_tv: TextView? = null

        init {
            pack_name_tv = itemView.findViewById(R.id.tv_adpdash_packname)
            lunch_tv = itemView.findViewById(R.id.tv_adpdash_lunch)
        }

        fun bindView(dashboard: Dashboard, holder: MyHolder) {

            holder.pack_name_tv?.text = dashboard.Pack_name
            holder.lunch_tv?.text = JsonParser.getCount(dashboard.Pack_id, orderList)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_dashboard, parent, false)
        return MyHolder(view, orderList)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        holder.bindView(list[position], holder);
        holder.itemView.setOnClickListener(View.OnClickListener {
            Utility.setFragment(
                FragmentMyOrder(ctx, ctx.getString(R.string.title_myorder)),
                ctx.getString(R.string.title_myorder),
                ctx as MainActivity
            )
        })
    }
}