package shree.techno.dabbaboyvendor.adapter

import android.R.attr.radius
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MenuActivity
import shree.techno.dabbaboyvendor.pojo.MealPackage


class MyMenuAdapter(var ctx: Context, var list: ArrayList<MealPackage>) :
    RecyclerView.Adapter<MyMenuAdapter.MyHolder>() {

    class MyHolder(itemview: View, var parent: ViewGroup) : RecyclerView.ViewHolder(itemview) {

        var menu_bt: Button? = null
        var name_tv: TextView? = null
        lateinit var myimage: ImageView

        fun bindView(pack: MealPackage) {
            menu_bt = itemView.findViewById(R.id.bt_adpmenu_menu)
            name_tv = itemView.findViewById(R.id.tv_adpmenu_packname)
            myimage = itemView.findViewById(R.id.iv_adpmenu_image)

            name_tv?.text = pack.name

            Glide.with(parent.context)
                .load(pack.image.replace(" ", "%20"))
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(10)))
                .into(myimage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view = LayoutInflater.from(ctx).inflate(R.layout.adp_mymenu, parent, false)
        return MyHolder(view, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(list.get(position))

        holder.menu_bt?.setOnClickListener(View.OnClickListener {
            ctx.startActivity(
                Intent(ctx, MenuActivity::class.java)
                    .putExtra("name", holder.name_tv?.text.toString())
                    .putExtra("pack_id", list.get(position).id)
            )
        })
    }
}