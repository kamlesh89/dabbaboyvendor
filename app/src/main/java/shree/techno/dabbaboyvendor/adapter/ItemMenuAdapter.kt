package shree.techno.dabbaboyvendor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import shree.techno.dabbaboyvendor.MenuItemClickListener
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.pojo.MenuItem

class ItemMenuAdapter(
    var ctx: Context,
    var list: ArrayList<MenuItem>,
    var listener: MenuItemClickListener
) :
    RecyclerView.Adapter<ItemMenuAdapter.MyHolder>() {


    class MyHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

        var name_tv: TextView? = null
        var edit_iv: ImageView? = null
        var delete_iv: ImageView? = null
        var status_iv: ImageView? = null

        init {
            name_tv = itemView.findViewById(R.id.tv_adpitem_name)
            edit_iv = itemView.findViewById(R.id.iv_adpitem_edit)
            delete_iv = itemView.findViewById(R.id.iv_adpitem_delete)
            status_iv = itemView.findViewById(R.id.iv_adpitem_status)
        }

        fun bindView(pack: MenuItem) {
            name_tv?.text = pack.item_name

//            status - 0 - pending
//            status - 1 - approved
//            status - 2 - dis approved
            if (pack.status.equals("1")) {
                status_iv?.setImageResource(R.drawable.ic_approved)
            } else if (pack.status.equals("2")) {
                status_iv?.setImageResource(R.drawable.ic_notapprove)
            } else {
                status_iv?.setImageResource(R.drawable.ic_pending)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view = LayoutInflater.from(ctx).inflate(R.layout.adp_itemview, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(list.get(position))
        holder.edit_iv?.setOnClickListener(View.OnClickListener {
            listener.menuItemClickOnEdit(list[position])
        })
        holder.delete_iv?.setOnClickListener(View.OnClickListener {
            listener.menuItemClickOnDelete(list[position])
        })
    }
}