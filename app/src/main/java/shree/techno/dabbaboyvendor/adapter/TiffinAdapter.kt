package shree.techno.dabbaboyvendor.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.pojo.Tiffin

class TiffinAdapter(var list: ArrayList<Tiffin>) : RecyclerView.Adapter<TiffinAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var code_tv: TextView? = null
        var status_tv: TextView? = null

        fun bindView(holder: MyHolder, tiffin: Tiffin) {

            code_tv = itemView.findViewById(R.id.tv_adptiffin_code)
            status_tv = itemView.findViewById(R.id.tv_adptiffin_status)

            code_tv?.setText("TIFFIN CODE : " + tiffin.code + tiffin.id)
            status_tv?.setText("Status : " + getStatusMessage(tiffin.status))
        }

        fun getStatusMessage(status: String): String {
            if (status.equals("0") || status.equals("6")) {
                return "Available in Kitchen"
            } else if (status.equals("1")) {
                return "Packed for Delivery"
            } else if (status.equals("2")) {
                return "Allote to Delivery boy"
            } else if (status.equals("3")) {
                return "Pick up by Delivery Boy For delivery"
            } else if (status.equals("4")) {
                return "Delivered to user"
            } else if (status.equals("5")) {
                return "Collected By Delivery Boy"
            } else {
                return "Not Available"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.adp_tiffin, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(holder, list[position])
    }
}