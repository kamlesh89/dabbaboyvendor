package shree.techno.dabbaboyvendor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import info.tech.dabbaboy.util.JsonParser
import shree.techno.dabbaboyvendor.HeaderItemClickListener
import shree.techno.dabbaboyvendor.MenuItemClickListener
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.pojo.PackHeader

class PackHeaderAdapter(
    var ctx: Context,
    var list: ArrayList<PackHeader>,
    var listener: HeaderItemClickListener,
    var menulistener: MenuItemClickListener
) :
    RecyclerView.Adapter<PackHeaderAdapter.MyHolder>() {

    class MyHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

        var name_tv: TextView? = null
        var nodata_tv: TextView? = null
        var add_bt: Button? = null
        var recyclerview: RecyclerView? = null

        fun bindView(pack: PackHeader) {
            recyclerview = itemView.findViewById(R.id.rv_adpheader_recycler)
            name_tv = itemView.findViewById(R.id.tv_adpheader_headername)
            nodata_tv = itemView.findViewById(R.id.tv_adpheader_notavailable)
            add_bt = itemView.findViewById(R.id.bt_adpheader_additem)

            name_tv?.text = pack.item_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view = LayoutInflater.from(ctx).inflate(R.layout.adp_packheader, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(list.get(position))
        var itemlist = JsonParser.getvendorItemlist(list[position])
        if (itemlist.size > 0) {
            holder.nodata_tv?.visibility = View.GONE
            holder.recyclerview?.visibility = View.VISIBLE
            holder.recyclerview?.setHasFixedSize(true)
            holder.recyclerview?.setLayoutManager(LinearLayoutManager(ctx))
            val adapter = ItemMenuAdapter(ctx, itemlist, menulistener)
            holder.recyclerview?.setAdapter(adapter)
        } else {
            holder.nodata_tv?.visibility = View.VISIBLE
            holder.recyclerview?.visibility = View.GONE
        }
        holder.add_bt?.setOnClickListener(View.OnClickListener {
            listener.clickonAdd(list[position])
        })
    }
}