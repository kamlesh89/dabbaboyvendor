package shree.techno.dabbaboyvendor.adapter

import android.app.Dialog
import android.content.Context
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.pojo.DeliveryBoy
import shree.techno.dabbaboyvendor.pojo.FreeOrder
import shree.techno.dabbaboyvendor.util.ConnectionDetector

class MyFreeOrderAdapter(
    var ctx: Context,
    var freelist: ArrayList<FreeOrder>,
    var type: String
) :
    RecyclerView.Adapter<MyFreeOrderAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var orderno_tv: TextView? = null
        var meal_tv: TextView? = null
        var status_tv: TextView? = null
        var status_bt: Button? = null
        var back_ll: LinearLayout? = null

        fun bindView(myOrder: FreeOrder) {

            orderno_tv = itemView.findViewById(R.id.tv_adpmyorder_orderno)
            meal_tv = itemView.findViewById(R.id.tv_adpmyorder_meal)
            status_tv = itemView.findViewById(R.id.tv_adpmyorder_status)
            status_bt = itemView.findViewById(R.id.bt_adpmyorder_statusbt)
            back_ll = itemView.findViewById(R.id.ll_adpmyorder_bg)

//            setBackcolor(myOrder.u_pay_status)
            orderno_tv?.setText("Order No. F-" + myOrder.id)
            meal_tv?.setText("Free Tiffin")
            status_tv?.setText(getStatustext(myOrder.status))
            status_bt?.setText(getbuttonText(myOrder.status))
            if (getbuttonText(myOrder.status).equals("")) {
                status_bt?.visibility = View.GONE
            }
        }

        fun getbuttonText(delivStatus: String): String {
            if (delivStatus.equals("0")) {
                return "Allote To"
            } else {
                return ""
            }
        }

        fun getStatustext(delivStatus: String): String {
            if (delivStatus.equals("0")) {
                return "Status - Not Packed"
            } else if (delivStatus.equals("1")) {
                return "Status - Packed"
            } else if (delivStatus.equals("2")) {
                return "Status - Alloted To Delivery Boy"
            } else if (delivStatus.equals("3")) {
                return "Status - Delivery Boy Picked Up"
            } else {
                return "Status - Delivered To User"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_myorder_view, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return freelist.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(freelist[position])
        holder.status_bt?.setOnClickListener(View.OnClickListener {
            if (ConnectionDetector.isConnected()) {
                loadData(ApiObjects.getMode("0"), freelist[position].id)
            }
        })
    }

    private fun loadData(myObject: JSONObject, id: String) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_DELOVEY_BOYS)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response, id)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject, id: String) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var data_obj = response.getJSONObject("data")
            var array_deliv_boy = data_obj.getJSONArray("boy_data")
            var deliv_list = JsonParser.getDeliveryboysforrecycler(array_deliv_boy)
            if (deliv_list!!.size > 0) {
                deliveryDialog(deliv_list!!, id)
            }
        } else {
            Utility.toastView(message, ctx)
        }
    }

    private fun deliveryDialog(
        delivList: ArrayList<DeliveryBoy>,
        id: String
    ) {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_assign_delivery)

        val displayMetrics = DisplayMetrics()
        (ctx as MainActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.window!!.attributes)
        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.4f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        dialog.window!!.attributes = layoutParams

        val recyclerView: RecyclerView = dialog.findViewById(R.id.rv_dialog_assigndelivboy)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(ctx)
        recyclerView?.adapter =
            MyDeliveryboyAdapter(ctx, delivList, id, dialog)
        dialog.show()
    }
}