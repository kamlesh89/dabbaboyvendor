package shree.techno.dabbaboyvendor.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.pojo.OrderHistory
import shree.techno.dabbaboyvendor.pojo.Tiffin

class OrderHistoryAdapter(var list: ArrayList<OrderHistory>) :
    RecyclerView.Adapter<OrderHistoryAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var packname_tv: TextView? = null
        var date_tv: TextView? = null
        var code_tv: TextView? = null
        var qty_tv: TextView? = null
        var status_tv: TextView? = null

        init {
            packname_tv = itemView.findViewById(R.id.tv_adphistory_packname)
            date_tv = itemView.findViewById(R.id.tv_adphistory_date)
            code_tv = itemView.findViewById(R.id.tv_adphistory_tiffincode)
            qty_tv = itemView.findViewById(R.id.tv_adphistory_qty)
            status_tv = itemView.findViewById(R.id.tv_adphistory_status)
        }

        fun bindView(holder: MyHolder, order: OrderHistory) {
            packname_tv?.setText(order.pack_name)
            date_tv?.setText(order.date)
            code_tv?.setText("Tiffin Code - " + order.tiffin_code)
            qty_tv?.setText("QTY - " + order.qty)
            status_tv?.setText(getStatusMessage(order.deliv_status))
        }

        fun getStatusMessage(status: String): String {
            if (status.equals("0")) {
                return "Not Delivered"
            } else if (status.equals("1")) {
                return "Packed for Delivery"
            } else if (status.equals("2")) {
                return "Allote to Delivery boy"
            } else if (status.equals("3")) {
                return "Pick up by Delivery Boy For delivery"
            } else if (status.equals("4") || status.equals("5") || status.equals("6")) {
                return "Delivered"
            } else {
                return "Not Available"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_order_history, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(holder, list[position])
    }
}