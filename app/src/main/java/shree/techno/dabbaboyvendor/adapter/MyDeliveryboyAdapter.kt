package shree.techno.dabbaboyvendor.adapter

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Constant
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_order_detail.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.fragment.FragmentMyOrder
import shree.techno.dabbaboyvendor.pojo.DeliveryBoy
import shree.techno.dabbaboyvendor.util.ConnectionDetector

class MyDeliveryboyAdapter(
    var ctx: Context,
    var boys_list: ArrayList<DeliveryBoy>,
    var id: String,
    var dialog: Dialog
) :
    RecyclerView.Adapter<MyDeliveryboyAdapter.MyHolder>() {


    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var name_tv: TextView? = null
        var mobile_tv: TextView? = null

        fun bindView(deliveryBoy: DeliveryBoy) {

            name_tv = itemView.findViewById(R.id.tv_adpspinner_name)
            mobile_tv = itemView.findViewById(R.id.tv_adpspinner_mobile)

            name_tv?.setText(deliveryBoy.name)
            mobile_tv?.setText(deliveryBoy.mobile)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_spinnerview, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return boys_list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(boys_list[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            if (ConnectionDetector.isConnected()) {
                dialog.dismiss()
                updateStatus(
                    ApiObjects.getupdateFreeDabbaStatus(id, boys_list[position].id, "2"),
                    WebApis.API_UPDATE_FREE_DABBA_STATUS
                )
            }
        })
    }

    private fun updateStatus(myObject: JSONObject, api: String) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(api)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            Utility.setFragment(
                FragmentMyOrder(ctx, ctx.getString(R.string.title_myorder)),
                ctx.getString(R.string.title_myorder),
                ctx as MainActivity
            )
        } else {
            Utility.toastView(message, ctx)
        }
    }

}