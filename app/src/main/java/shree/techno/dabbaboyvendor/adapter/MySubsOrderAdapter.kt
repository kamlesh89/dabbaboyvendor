package shree.techno.dabbaboyvendor.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.util.DisplayMetrics
import android.view.*
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.DisposalOrderDetailActivity
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.activity.OrderDetailActivity
import shree.techno.dabbaboyvendor.pojo.DeliveryBoy
import shree.techno.dabbaboyvendor.pojo.TodaysOrders
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class MySubsOrderAdapter(
    var ctx: Context,
    var list: ArrayList<TodaysOrders>,
    var type: String
) :
    RecyclerView.Adapter<MySubsOrderAdapter.MyHolder>() {

    val INTENT_ORDER_DETAIL = 101
    var sessionManager: SessionManager? = null

    init {
        sessionManager = SessionManager(ctx)
    }

    class MyHolder(itemView: View, var ctx: Context) : RecyclerView.ViewHolder(itemView) {

        var orderno_tv: TextView? = null
        var meal_tv: TextView? = null
        var status_tv: TextView? = null
        var tiffin_type_tv: TextView? = null
        var status_bt: Button? = null
        var change_db_bt: Button? = null
        var back_ll: LinearLayout? = null

        fun bindView(myOrder: TodaysOrders) {

            orderno_tv = itemView.findViewById(R.id.tv_adpmyorder_orderno)
            meal_tv = itemView.findViewById(R.id.tv_adpmyorder_meal)
            status_tv = itemView.findViewById(R.id.tv_adpmyorder_status)
            tiffin_type_tv = itemView.findViewById(R.id.tv_adpmyorder_tiffintype)
            status_bt = itemView.findViewById(R.id.bt_adpmyorder_statusbt)
            change_db_bt = itemView.findViewById(R.id.bt_adpmyorder_changedelivery)
            back_ll = itemView.findViewById(R.id.ll_adpmyorder_bg)

//            setBackcolor(myOrder.u_pay_status)
            orderno_tv?.setText("Order No. " + myOrder.order_id)
            meal_tv?.setText(myOrder.pack_name)
            status_tv?.setText(getStatustext(myOrder.deliv_status))
            status_bt?.setText(getbuttonText(myOrder.deliv_status))
            tiffin_type_tv?.setText(myOrder.tiffin_type)
            if (getbuttonText(myOrder.deliv_status).equals("")) {
                status_bt?.visibility = View.GONE
            }
            var status: Int = myOrder.deliv_status.toInt()
            if (status >= 2) {
                status_bt?.isClickable = false
            }

            // change delivery boy
            if (myOrder.deliv_status.equals("2")) {
                change_db_bt?.visibility = View.VISIBLE
            } else {
                change_db_bt?.visibility = View.GONE
            }
        }

        fun getbuttonText(delivStatus: String): String {
            if (delivStatus.equals("0")) {
                return "Ready"
            } else if (delivStatus.equals("1")) {
                return "Allote To"
            } else {
                return ""
            }
        }

        fun getStatustext(delivStatus: String): String {
            if (delivStatus.equals("0")) {
                return "Status - Not Packed"
            } else if (delivStatus.equals("1")) {
                return "Status - Packed"
            } else if (delivStatus.equals("2")) {
                return "Status - Alloted To Delivery Boy"
            } else if (delivStatus.equals("3")) {
                return "Status - Delivery Boy Picked Up"
            } else if (delivStatus.equals("4")) {
                return "Status - Delivered To User"
            } else if (delivStatus.equals("5")) {
                return "Status - Picked from User"
            } else if (delivStatus.equals("6")) {
                return "Status - Collected Tiffin Box"
            } else {
                return ""
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_myorder_view, parent, false)
        return MyHolder(view, ctx)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(list[position])
        holder.change_db_bt?.setOnClickListener(View.OnClickListener {
            if (ConnectionDetector.isConnected()) {
                var body = ApiObjects.getTodaysOrderMenu(
                    sessionManager!!.getData(SessionManager.KEY_ID),
                    type,
                    list[position].date,
                    list[position].order_id,
                    list[position].pack_id
                )
                loadData(body,position)
            }
        })
        holder.status_bt?.setOnClickListener(View.OnClickListener {
            if (list[position].tiffin_type.equals("Hot Dabba")) {
                (ctx as MainActivity).startActivityForResult(
                    Intent(ctx, OrderDetailActivity::class.java)
                        .putExtra("data", getData(position))
                    , INTENT_ORDER_DETAIL
                )
            } else {
                (ctx as MainActivity).startActivityForResult(
                    Intent(ctx, DisposalOrderDetailActivity::class.java)
                        .putExtra("data", getData(position))
                    , INTENT_ORDER_DETAIL
                )
            }
        })
        holder.itemView?.setOnClickListener(View.OnClickListener {
            var deliv_status = list[position].deliv_status
            if (!deliv_status.equals("0") && !deliv_status.equals("1")) {
                ctx.startActivity(
                    Intent(ctx, OrderDetailActivity::class.java)
                        .putExtra("data", getData(position))
                )
            }
        })
    }

    private fun getData(position: Int): String? {
        var obj = JSONObject()
        var order = list[position]
        obj.put("id", order.id)
        obj.put("order_id", order.order_id)
        obj.put("pack_id", order.pack_id)
        obj.put("pack_name", order.pack_name)
        obj.put("type", type)
        obj.put("date", order.date)
        obj.put("qty", order.qty)
        obj.put("time", order.time)
        obj.put("tiffin_code", order.tiffin_code)
        obj.put("status", order.deliv_status)
        obj.put("address", order.address_object)
        obj.put("user", order.user_object)
        obj.put("tiffin_type", order.tiffin_type)
        return obj.toString()
    }

    // get delivery boy details
    private fun loadData(apibody: JSONObject, id: Int) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_TODAY_ORDER_MENU)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response,id)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject,position: Int) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            setLoadData(response,position)
        } else {
            Utility.toastView(message, ctx)
        }
    }

    private fun setLoadData(response: JSONObject, position: Int) {
        var object_data = response.getJSONObject("data")
        var array_deliv_boy = object_data.getJSONArray("delivery_boy")
        var deliv_boy_list = JsonParser.getDeliveryboyslist(array_deliv_boy)
        showDBDialog(deliv_boy_list,position)
    }

    private fun showDBDialog(
        delivBoyList: ArrayList<DeliveryBoy>,
        position: Int
    ) {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_assign_delivery)

        val displayMetrics = DisplayMetrics()
        (ctx as MainActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.window!!.attributes)
        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.4f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        dialog.window!!.attributes = layoutParams

        val recyclerView: RecyclerView = dialog.findViewById(R.id.rv_dialog_assigndelivboy)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(ctx)
        recyclerView?.adapter = ChangeDeliveryboyAdapter(ctx, delivBoyList, list[position].id, dialog, type,list[position].tiffin_code)
        dialog.show()
    }

}