package shree.techno.dabbaboyvendor

import shree.techno.dabbaboyvendor.pojo.MenuItem

interface MenuItemClickListener {

    fun menuItemClickOnEdit(menuItem: MenuItem)
    fun menuItemClickOnDelete(menuItem: MenuItem)

}