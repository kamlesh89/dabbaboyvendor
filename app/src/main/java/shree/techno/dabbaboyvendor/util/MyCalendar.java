package shree.techno.dabbaboyvendor.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MyCalendar {
    public static final ArrayList<String> getnext15Days() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 14; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, i); //add days
            Date date = cal.getTime();
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String mydate = dateformat.format(date);
            list.add(mydate);
        }
        return list;
    }

}
