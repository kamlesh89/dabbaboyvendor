package info.tech.dabbaboy.util

class WebApis {
//    http://lakesideayurveda.com/dabba_boy/Home/vendor_api_page

    companion object {

        //        val BASE_URL = "http://lakesideayurveda.com/dabba_boy/Api/"
        val BASE_URL = "https://shreetechno.com/dabba_boy/Api/"
        val API_LOGIN = BASE_URL + "vendorLogin"
        val API_FORGOT = BASE_URL + ""
        val API_PACKAGE = BASE_URL + "allHeaderDetail"
        val API_PACKAGE_HEADER = BASE_URL + "getAllHeaderDataByVendor"

        val API_ADD_MENU = BASE_URL + "addHeaderItemDetailByVendor"

        // get all tiffin
        val API_TIFFIN = BASE_URL + "getAlltiffinCodeByVendor"

        // today order order
        val API_TODAY_ORDER = BASE_URL + "getUserDetailByVendorIdAndDate"
        val API_TODAY_ORDER_MENU = BASE_URL + "getUserFixUnfixMenu"

        //update status for ready
        val API_TIFFIN_READY = BASE_URL + "updateStatusforReady"

        //update status for disposal ready
        val API_DISPOSAL_TIFFIN_READY = BASE_URL + "updateStatusforDisposalTiffin"

        // order history
        val API_ORDER_HISTORY = BASE_URL + "gethistoryOrderDetail"

        // free dabba update
        val API_UPDATE_FREE_DABBA_STATUS = BASE_URL + "updateFreeDabbaTiffin"

        // get Dilivery Boy Detail
        val API_DELOVEY_BOYS = BASE_URL + "getDiliveryBoyDetail"

        // update Dilivery Boy location
        val API_DELOVEY_BOYS_UPDATE_LOCATION = BASE_URL + "addDiliveryBoyAddress"


        // delivery boys apis
        val API_DEL_LOGIN = BASE_URL + "diliveryBoyLogin"
        val API_DEL_ORDERS = BASE_URL + "getDeliveryVendorDetail"
        val API_UPDATE_STATUS = BASE_URL + "updateStatusforReady"
    }
}
