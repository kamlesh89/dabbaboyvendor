package info.tech.dabbaboy.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.androidnetworking.BuildConfig
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.deliveryboy.DeliveryMainActivity
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class Utility {
    companion object {

        lateinit var dialog: Dialog

        @JvmStatic
        fun showLoader(ctx: Context) {
            dialog = Dialog(ctx);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.layout_progress);
            dialog.show()
        }

        @JvmStatic
        fun hideLoader() {
            if (dialog.isShowing) {
                dialog.dismiss()
            }
        }

        fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun setFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as MainActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, tag)
                .commit()
        }

        fun getDate(): String {
            val c = Calendar.getInstance().time
            val df = SimpleDateFormat("yyyy-MM-dd")
            return df.format(c)
        }

        // get address from object
        fun getAddress(lunchAddress: JSONObject?): String {
            var builder = StringBuilder()
            builder.append(lunchAddress?.getString("building_name") + " ")
            builder.append(lunchAddress?.getString("street") + " ")
            builder.append(lunchAddress?.getString("landmark") + " ")
            builder.append(lunchAddress?.getString("area"))
            return builder.toString()
        }


/*
        fun setFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as MainActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, tag)
                .commit()
        }
*/


        @JvmStatic
        fun toastView(msg: String, ctx: Context) {
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
        }

        fun referFriend(ctx: Context, code: String) {

            var content =
                "Install DabbaBoy application using my refer code : " + code + "\n\nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, content);
            ctx.startActivity(Intent.createChooser(shareIntent, "Share via"))
        }

        @JvmStatic
        fun snacbarShow(msg: String, coordinatorLayout: CoordinatorLayout) {
            val snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG).show()
        }

        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view: View? = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }


        // delivery utility
        fun setDeliveryFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as DeliveryMainActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_delmain_frame, fragment, tag)
                .commit()
        }

    }
}