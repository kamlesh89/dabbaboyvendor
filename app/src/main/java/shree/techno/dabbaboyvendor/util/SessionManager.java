package shree.techno.dabbaboyvendor.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.app.ActivityCompat;

import info.tech.dabbaboy.util.Constant;
import shree.techno.dabbaboyvendor.activity.LoginTypeActivity;
import shree.techno.dabbaboyvendor.activity.MainActivity;


public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context ctx;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "sessionmanager";
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL_ID = "email";
    public static final String KEY_MOBILE = "mobile";

    public static final String KEY_AREA = "area";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_TYPE = "type";

    public SessionManager(Context context) {
        this.ctx = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String id, String name, String email, String mobile, String area, String pin, int type) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL_ID, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_AREA, area);
        editor.putString(KEY_PINCODE, pin);
        editor.putInt(KEY_TYPE, type);
        editor.commit();
    }

    public void setData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(ctx, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(i);
        }
    }

    public void logoutUser(Activity mainActivity) {
        editor.clear();
        editor.commit();
        ActivityCompat.finishAffinity(mainActivity);
        Intent i = new Intent(ctx.getApplicationContext(), LoginTypeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public String getData(String key) {
        return pref.getString(key, null);
    }

    public int getIntData(String key) {
        return pref.getInt(key, 0);
    }
}
