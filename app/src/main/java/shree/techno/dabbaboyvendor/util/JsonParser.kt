package info.tech.dabbaboy.util

import org.json.JSONArray
import org.json.JSONObject
import shree.techno.dabbaboyvendor.deliveryboy.pojo.DeliveryOrderDetails
import shree.techno.dabbaboyvendor.pojo.*
import java.util.*
import kotlin.collections.ArrayList

class JsonParser {

    companion object {

        // tiifin list
        fun getAllTiffinlist(array: JSONArray): ArrayList<Tiffin> {
            var list = ArrayList<Tiffin>()
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var id = obj.getString("id")
                    var vendor_id = obj.getString("vendor_id")
                    var tiffin_code = obj.getString("tiffin_code")
                    var status = obj.getString("status")
                    var u_name = obj.getString("user_name")
                    var u_mobile = obj.getString("user_mobile")
                    var u_id = obj.getString("user_id")
                    var d_name = obj.getString("delivery_boy_name")
                    var d_mobile = obj.getString("delivery_mobile")
                    var d_id = obj.getString("delivery_id")
                    if (tiffin_code.contains("DBT")) {
                        list.add(
                            Tiffin(
                                id, vendor_id, tiffin_code, status, u_name, u_mobile
                                , u_id, d_name, d_mobile, d_id
                            )
                        )
                    }
                }
            }
            return list
        }

        // package list
        fun getpackagelist(array: JSONArray): ArrayList<MealPackage> {
            var list = ArrayList<MealPackage>()
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var id = obj.getString("id")
                    var name = obj.getString("menu_name")
                    var image = obj.getString("package_image")
                    var varient = obj.getJSONArray("variant")
                    var adminpack = obj.getJSONArray("vendorpack")
                    list.add(MealPackage(id, name, image, varient, adminpack))
                }
            }
            return list
        }

        // get header packs
        fun getpackHeader(dataarray: JSONArray): ArrayList<PackHeader> {
            var list = ArrayList<PackHeader>()
            if (dataarray.length() > 0) {
                for (i in 0..dataarray.length() - 1) {
                    var obj = dataarray.getJSONObject(i)

                    var variation_type = obj.getString("Variation_type")
                    if (!variation_type.equals("Fix")) {
                        var id = obj.getString("header_id")
                        var pack_id = obj.getString("package_id")
                        var type = obj.getString("service_type")
                        var name = obj.getString("item_name")
                        var created_date_time = obj.getString("created_data_time")
                        var vendor_items = obj.getJSONArray("vendoritems")
                        list.add(
                            PackHeader(
                                id, type, pack_id, name, variation_type,
                                created_date_time, vendor_items
                            )
                        )
                    }
                }
            }
            return list
        }

        // get fix items
        fun getFixItems(dataarray: JSONArray): ArrayList<PackHeader> {
            var list = ArrayList<PackHeader>()
            if (dataarray.length() > 0) {
                for (i in 0..dataarray.length() - 1) {
                    var obj = dataarray.getJSONObject(i)
                    var variation_type = obj.getString("Variation_type")
                    if (variation_type.equals("Fix")) {
                        var id = obj.getString("header_id")
                        var pack_id = obj.getString("package_id")
                        var type = obj.getString("service_type")
                        if (type.equals("Launch")) {
                            type = "Lunch"
                        }
                        var name = obj.getString("item_name")
                        var created_date_time = obj.getString("created_data_time")
                        var vendor_items = obj.getJSONArray("vendoritems")
                        list.add(
                            PackHeader(
                                id, type, pack_id, name, variation_type,
                                created_date_time, vendor_items
                            )
                        )
                    }
                }
            }
            return list
        }

        // get fix header string
        fun getfixHeader(list: ArrayList<PackHeader>): String {
            var builder = StringBuilder()
            for (i in 0..list.size - 1) {
                var variation_type = list.get(i).variation
                if (variation_type.equals("Fix")) {
                    builder?.append(variation_type)
                    if (i < list.size - 1) {
                        builder?.append(", ")
                    }
                }
            }
            return builder.toString()
        }

        fun getvendorItemlist(packHeader: PackHeader): ArrayList<MenuItem> {
            var list = ArrayList<MenuItem>()
            var dataarray = packHeader.vendorItem
            if (dataarray.length() > 0) {
                for (i in 0..dataarray.length() - 1) {
                    var obj = dataarray.getJSONObject(i)
                    var id = obj.getString("id")
                    var name = obj.getString("item_name")
                    var price = obj.getString("price")
                    var qty = obj.getString("qty")
                    var service_type = obj.getString("service_type")
                    var status = obj.getString("status")
                    list.add(
                        MenuItem(
                            id, name, price, qty, packHeader.pack_id,
                            packHeader.id, service_type, status
                        )
                    )
                }
            }
            return list
        }

        fun getFixitemstring(filterFixlist: ArrayList<PackHeader>): String {
            var builder = StringBuilder()
            if (filterFixlist.size > 0) {
                for (i in 0..filterFixlist.size - 1) {
                    builder.append(filterFixlist[i].item_name)
                    if (i < filterFixlist.size - 1) {
                        builder.append(", ")
                    }
                }
            }
            return builder.toString()
        }

        // get dashboard list for lunch order
        fun getDashboardlist(arrayData: JSONArray, type: String): ArrayList<Dashboard> {
            var list = ArrayList<Dashboard>()
            var id_list = ArrayList<String>()

            if (arrayData.length() > 0) {
                for (i in 0..arrayData.length() - 1) {
                    var obj = arrayData.getJSONObject(i)
                    var id = obj.getString("package_id")
                    if (!id_list.contains(id)) {
                        id_list.add(id)
                        list.add(
                            Dashboard(
                                obj.getString("package_id"),
                                obj.getString("package_name"),
                                type
                            )
                        )
                    }
                }
            }
            return list
        }

        // get todays lunch order
        fun getTodayLunchlist(arrayData: JSONArray): ArrayList<TodaysOrders> {
            var list = ArrayList<TodaysOrders>()
            if (arrayData.length() > 0) {
                for (i in 0..arrayData.length() - 1) {
                    var obj = arrayData.getJSONObject(i)
                    list.add(
                        TodaysOrders(
                            obj.getString("id"),
                            obj.getString("order_id"),
                            obj.getString("user_id"),
                            obj.getString("date"),
                            obj.getString("lunch_time"),
                            obj.getString("lunch_qty"),
                            obj.getString("lunch_vendor_id"),
                            obj.getString("lunch_status"),
                            obj.getString("lunch_address_id"),
                            obj.getString("package_name"),
                            obj.getString("package_id"),
                            obj.getString("lunch_tiffin_status"),
                            obj.getString("lunch_tiffin_code"),
                            obj.getJSONObject("LunchAddress"),
                            obj.getJSONObject("user_detail"),
                            obj.getString("tiffin_type")
                        )
                    )
                }
            }
            return list
        }

        // get todays dinner order
        fun getTodayDinnerlist(arrayData: JSONArray): ArrayList<TodaysOrders> {
            var list = ArrayList<TodaysOrders>()
            if (arrayData.length() > 0) {
                for (i in 0..arrayData.length() - 1) {
                    var obj = arrayData.getJSONObject(i)
                    list.add(
                        TodaysOrders(
                            obj.getString("id"),
                            obj.getString("order_id"),
                            obj.getString("user_id"),
                            obj.getString("date"),
                            obj.getString("dinner_time"),
                            obj.getString("dinner_qty"),
                            obj.getString("dinner_vendor_id"),
                            obj.getString("dinner_status"),
                            obj.getString("dinner_address_id"),
                            obj.getString("package_name"),
                            obj.getString("package_id"),
                            obj.getString("dinner_tiffin_status"),
                            obj.getString("dinner_tiffin_code"),
                            obj.getJSONObject("dinner_address"),
                            obj.getJSONObject("user_detail"),
                            obj.getString("tiffin_type")
                        )
                    )
                }
            }
            return list
        }

        // free order
        fun getTodayfreeOrder(arrayData: JSONArray, type: String): ArrayList<FreeOrder> {
            var list = ArrayList<FreeOrder>()
            if (arrayData.length() > 0) {
                for (i in 0..arrayData.length() - 1) {
                    var obj = arrayData.getJSONObject(i)
                    var obj_type = obj.getString("service_type")
                    if (type.equals(obj_type)) {
                        list.add(
                            FreeOrder(
                                obj.getString("id"),
                                obj.getString("user_id"),
                                obj.getString("delivery_time"),
                                obj.getString("service_type"),
                                obj.getString("address_id"),
                                obj.getString("status"),
                                obj.getString("order_date"),
                                obj.getString("vendor_id"),
                                obj.getString("delivery_boy_id")
                            )
                        )
                    }
                }
            }
            return list
        }

        // get selected and fix menu as string
        fun getslectedItems(arrayFix: JSONArray, arraySelected: JSONArray): String {
            var builder = StringBuilder()
            if (arrayFix.length() > 0) {
                for (i in 0..arrayFix.length() - 1) {
                    var obj = arrayFix.getJSONObject(i)
                    builder.append(obj.getString("item_name") + ", ")
                }
            }
            if (arraySelected.length() > 0) {
                for (i in 0..arraySelected.length() - 1) {
                    var obj = arraySelected.getJSONObject(i)
                    builder.append(obj.getString("item_name"))
                    if (i != arraySelected.length() - 1) {
                        builder.append(", ")
                    }
                }
            }
            return builder.toString()
        }

        // get tiffin code
        fun getKitchentiffines(arrayTiffin: JSONArray): ArrayList<String> {
            var list = ArrayList<String>()
            list.add("Select Tiffin Code")
            if (arrayTiffin.length() > 0) {
                for (i in 0..arrayTiffin.length() - 1) {
                    var obj = arrayTiffin.getJSONObject(i)
                    if (obj.getString("status").equals("0") || obj.getString("status")
                            .equals("6")
                    ) {
                        list.add(obj.getString("tiffin_code"))
                    }
                }
            }
            return list
        }

        // get disposal tiffin code
        fun getDisposaltiffineCode(): ArrayList<String> {
            var list = ArrayList<String>()
            list.add("Select Disposal Code")
            list.add("DIS-" + mycode())
            return list
        }

        private fun mycode(): String {
            val r = Random()
            val codes: MutableList<Int> = ArrayList()
            for (i in 0..9) {
                var x: Int = r.nextInt(9999)
                while (codes.contains(x)) x = r.nextInt(9999)
                codes.add(x)
            }
            val str = String.format("%04d", codes[0])
            return str
        }


        fun getDeliveryboys(arrayDeliv: JSONArray): ArrayList<DeliveryBoy> {
            var list = ArrayList<DeliveryBoy>()
            list.add(DeliveryBoy("", "Select Delivery Boy", "", "", "", ""))
            if (arrayDeliv.length() > 0) {
                for (i in 0..arrayDeliv.length() - 1) {
                    var obj = arrayDeliv.getJSONObject(i)
                    list.add(
                        DeliveryBoy(
                            obj.getString("id"),
                            obj.getString("dilivery_boy_name"),
                            obj.getString("email_id"),
                            obj.getString("mobile_no"),
                            obj.getString("aadhar_no"),
                            obj.getString("boy_image")
                        )
                    )
                }
            }
            return list
        }

        fun getDeliveryboyslist(arrayDeliv: JSONArray): ArrayList<DeliveryBoy> {
            var list = ArrayList<DeliveryBoy>()
            if (arrayDeliv.length() > 0) {
                for (i in 0..arrayDeliv.length() - 1) {
                    var obj = arrayDeliv.getJSONObject(i)
                    list.add(
                        DeliveryBoy(
                            obj.getString("id"),
                            obj.getString("dilivery_boy_name"),
                            obj.getString("email_id"),
                            obj.getString("mobile_no"),
                            obj.getString("aadhar_no"),
                            obj.getString("boy_image")
                        )
                    )
                }
            }
            return list
        }

        // delivery boy for recycler view
        fun getDeliveryboysforrecycler(arrayDeliv: JSONArray): ArrayList<DeliveryBoy> {
            var list = ArrayList<DeliveryBoy>()
            if (arrayDeliv.length() > 0) {
                for (i in 0..arrayDeliv.length() - 1) {
                    var obj = arrayDeliv.getJSONObject(i)
                    list.add(
                        DeliveryBoy(
                            obj.getString("id"),
                            obj.getString("dilivery_boy_name"),
                            obj.getString("email_id"),
                            obj.getString("mobile_no"),
                            obj.getString("aadhar_no"),
                            obj.getString("boy_image")
                        )
                    )
                }
            }
            return list
        }

        // get dashboard count
        fun getCount(id: String, orderList: ArrayList<TodaysOrders>): String {
            var no: Int = 0
            for (i in 0..orderList.size - 1) {
                if (orderList[i].pack_id.equals(id)) {
                    no = ++no
                }
            }
            return no.toString()
        }

        // for delivery boy system
        fun getDelOrderlist(
            arrayData: JSONArray,
            from_latitude: Double,
            from_longitude: Double
        ): ArrayList<DeliveryOrderDetails> {
            var list = ArrayList<DeliveryOrderDetails>()
            for (i in 0..arrayData.length() - 1) {
                var obj = arrayData.getJSONObject(i)
                var status = obj.getString("status")
                if (!status.equals("0") || !status.equals("1")) {
                    list.add(
                        DeliveryOrderDetails(
                            obj.getString("id"),
                            obj.getString("tiffin_code"),
                            obj.getString("status"),
                            obj.getString("user_name"),
                            obj.getString("user_mobile"),
                            obj.getString("daily_order_id"),
                            obj.getString("service_type"),
                            obj.getJSONObject("addressDetail"),
                            obj.getJSONObject("vendorDetail"),
                            getlatitude(obj.getJSONObject("addressDetail")),
                            getlongitude(obj.getJSONObject("addressDetail")),
                            getDistance(
                                from_latitude,
                                from_longitude,
                                getlatitude(obj.getJSONObject("addressDetail")),
                                getlongitude(obj.getJSONObject("addressDetail"))
                            )
                        )
                    )
                }
            }
            return list
        }

        private fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
            val theta = lon1 - lon2
            var dist = (Math.sin(deg2rad(lat1))
                    * Math.sin(deg2rad(lat2))
                    + (Math.cos(deg2rad(lat1))
                    * Math.cos(deg2rad(lat2))
                    * Math.cos(deg2rad(theta))))
            dist = Math.acos(dist)
            dist = rad2deg(dist)
            dist = dist * 60 * 1.1515 * 1.8
            return String.format("%.1f", dist).toDouble()
        }

        private fun deg2rad(deg: Double): Double {
            return deg * Math.PI / 180.0
        }

        private fun rad2deg(rad: Double): Double {
            return rad * 180.0 / Math.PI
        }

        private fun getlatitude(jsonObject: JSONObject): Double {
            return jsonObject.getDouble("latitude")
        }

        private fun getlongitude(jsonObject: JSONObject): Double {
            return jsonObject.getDouble("longitude")
        }

        // for delivery boy for free
        fun getDelfreeOrderlist(
            arrayData: JSONArray,
            from_latitude: Double,
            from_longitude: Double
        ): ArrayList<DeliveryOrderDetails> {
            var list = ArrayList<DeliveryOrderDetails>()
            for (i in 0..arrayData.length() - 1) {
                var obj = arrayData.getJSONObject(i)
                list.add(
                    DeliveryOrderDetails(
                        obj.getString("id"),
                        "",
                        obj.getString("status"),
                        obj.getString("name"),
                        obj.getString("mobile_no"),
                        "",
                        obj.getString("service_type"),
                        obj.getJSONObject("address_detail"),
                        obj.getJSONObject("vendor_detail"),
                        getlatitude(obj.getJSONObject("addressDetail")),
                        getlongitude(obj.getJSONObject("addressDetail")),
                        getDistance(
                            from_latitude,
                            from_longitude,
                            getlatitude(obj.getJSONObject("addressDetail")),
                            getlongitude(obj.getJSONObject("addressDetail"))
                        )
                    )
                )
            }
            return list
        }

        private fun getRandomNumber(): Float {
            val r = Random()
            val codes: MutableList<Int> = ArrayList()
            for (i in 0..9) {
                var x: Int = r.nextInt(9999)
                while (codes.contains(x)) x = r.nextInt(9999)
                codes.add(x)
            }
            val str = String.format("%02d", codes[0])
            return str.toFloat()
        }

        fun getdinnerOrderHistory(arrayData: JSONArray): java.util.ArrayList<OrderHistory>? {
            var list = ArrayList<OrderHistory>()
            for (i in 0..arrayData.length() - 1) {
                var obj = arrayData.getJSONObject(i)
                list.add(
                    OrderHistory(
                        obj.getString("id"),
                        obj.getString("package_name"),
                        obj.getString("date"),
                        obj.getString("dinner_tiffin_code"),
                        obj.getString("dinner_tiffin_status"),
                        obj.getString("dinner_delivery_status"),
                        obj.getString("dinner_qty")
                    )
                )
            }
            return list
        }

        fun getlunchOrderHistory(arrayData: JSONArray): java.util.ArrayList<OrderHistory>? {
            var list = ArrayList<OrderHistory>()
            for (i in 0..arrayData.length() - 1) {
                var obj = arrayData.getJSONObject(i)
                list.add(
                    OrderHistory(
                        obj.getString("order_id"),
                        obj.getString("package_name"),
                        obj.getString("date"),
                        obj.getString("lunch_tiffin_code"),
                        obj.getString("lunch_tiffin_status"),
                        obj.getString("lunch_delivery_status"),
                        obj.getString("lunch_qty")
                    )
                )
            }
            return list
        }
    }
}