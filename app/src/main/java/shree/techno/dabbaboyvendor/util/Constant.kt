package info.tech.dabbaboy.util

class Constant {

    companion object {

        val SPLASH_TIMEOUT: Long = 3000

        val LOGIN_TYPE_VENDOR: Int = 100;
        val LOGIN_TYPE_DELIVERY: Int = 101;

        val DELIVERY_TITLE_MY_ORDER = "Order For Delivery"
        val DELIVERY_TITLE_MY_PROFILE = "My Profile"

        // delivery bot adapter type
        val FREE_DELIIVERY_BOY = "Free"
        val CHANGE_DELIVERY_BOY = "Change"
    }
}