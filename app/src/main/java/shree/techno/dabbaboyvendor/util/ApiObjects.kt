package info.tech.dabbaboy.util

import org.json.JSONObject
import shree.techno.dabbaboyvendor.pojo.MenuItem
import shree.techno.dabbaboyvendor.pojo.PackHeader

class ApiObjects {
    companion object {

        // get only mode
        fun getMode(mode: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", mode)
            return myobject
        }

        // get today order
        fun getTodaysOrder(vendor_id: String, service_type: String, orde_date: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("vendor_id", vendor_id)
            myobject.put("service_type", service_type)
            myobject.put("order_date", orde_date)
            return myobject
        }

        // get today order menu
        fun getTodaysOrderMenu(
            vendor_id: String,
            service_type: String,
            orde_date: String,
            order_id: String,
            pack_id: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("vendor_id", vendor_id)
            myobject.put("service_type", service_type)
            myobject.put("order_date", orde_date)
            myobject.put("order_id", order_id)
            myobject.put("package_id", pack_id)
            return myobject
        }

        // get package header
        fun getpackHeaders(pack_id: String, vendor_id: String, date: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("package_id", pack_id)
            myobject.put("vendor_id", vendor_id)
            myobject.put("item_date", date)
            return myobject
        }

        // vendor login
        fun getLogin(mobile: String, pass: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("mobile_no", mobile)
            myobject.put("password", pass)
            return myobject
        }

        // add menu
        fun addMenuItem(menuItem: MenuItem, vendor_id: String, date: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("item_date", date)
            myobject.put("package_id", menuItem.pack_id)
            myobject.put("vendor_id", vendor_id)
            myobject.put("title_id", menuItem.header_id)
            myobject.put("service_type", menuItem.service_type)
            myobject.put("item_name", menuItem.item_name)
            myobject.put("price", menuItem.item_price)
            myobject.put("qty", menuItem.item_qty)
            return myobject
        }

        // edit menu item
        fun editMenuItem(menuItem: MenuItem, date: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("item_date", date)
            myobject.put("item_id", menuItem.item_id)
            myobject.put("item_name", menuItem.item_name)
            myobject.put("price", menuItem.item_price)
            myobject.put("qty", menuItem.item_qty)
            return myobject
        }

        // delete menu item
        fun deleteMenuItem(item_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "2")
            myobject.put("item_id", item_id)
            return myobject
        }

        // get all menu item
        fun getallMenuItem(vendor_id: String, pack_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "3")
            myobject.put("vendor_id", vendor_id)
            myobject.put("package_id", pack_id)
            return myobject
        }

        fun getTiifinBody(vendor_id: String?): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("vendor_id", vendor_id)
            return myobject
        }

        // update free dabba status
        fun getupdateFreeDabbaStatus(id: String?, deliv_id: String?, status: String?): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("id", id)
            myobject.put("delivery_id", deliv_id)
            myobject.put("status", status)
            return myobject
        }


        // get ready update for status - 0
        fun getReadyupadte(orderdate_id: String, type: String, tiffincode: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("id", orderdate_id)
            myobject.put("service_type", type)
            myobject.put("status", "1")
            myobject.put("tiffin_code", tiffincode)
            return myobject
        }

        // get ready update ( disposal ) for status - 0
        fun getdisposalReadyupadte(
            orderdate_id: String,
            type: String,
            tiffincode: String,
            vendor_id: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("id", orderdate_id)
            myobject.put("service_type", type)
            myobject.put("status", "1")
            myobject.put("tiffin_code", tiffincode)
            myobject.put("vendor_id", vendor_id)
            return myobject
        }


        // get assign delivery boy status - 1
        fun getAssignDeliveyboy(
            orderdate_id: String,
            type: String,
            delv_boy_id: String,
            tiffincode: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("id", orderdate_id)
            myobject.put("service_type", type)
            myobject.put("status", "2")
            myobject.put("delivery_boy_id", delv_boy_id)
            myobject.put("tiffin_code", tiffincode)
            return myobject
        }

        // get assign delivery boy for disposal status - 1
        fun getAssigndisposalDeliveyboy(
            orderdate_id: String,
            type: String,
            delv_boy_id: String,
            tiffincode: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("id", orderdate_id)
            myobject.put("service_type", type)
            myobject.put("status", "2")
            myobject.put("delivery_boy_id", delv_boy_id)
            myobject.put("tiffin_code", tiffincode)
            return myobject
        }


        /* Delivery boy api objects */

        // getOrder
        fun getDeliverBoyOrder(boy_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("delivery_boy_id", boy_id)
            return myobject
        }

        // update status from delivery boy
        fun upadteDeliveryStatus(
            orderdate_id: String,
            type: String,
            status: String,
            tiffincode: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("id", orderdate_id)
            myobject.put("service_type", type)
            myobject.put("status", status)
            myobject.put("tiffin_code", tiffincode)
            return myobject
        }

        fun updateboyLocation(id: String, lati: Double, longi: Double): JSONObject? {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("dilivery_boy_id", id)
            myobject.put("latitude", lati)
            myobject.put("longitude", longi)
            return myobject
        }

    }
}