package shree.techno.dabbaboyvendor.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import info.tech.dabbaboy.util.Constant
import kotlinx.android.synthetic.main.activity_login_type.*
import kotlinx.android.synthetic.main.activity_splash.*
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.deliveryboy.DeliveryMainActivity
import shree.techno.dabbaboyvendor.util.SessionManager

class LoginTypeActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
        checkLogin()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        bt_logintype_vendor.setOnClickListener(this)
        bt_logintype_delivery.setOnClickListener(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login_type
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_logintype_vendor -> {
                openLogin(Constant.LOGIN_TYPE_VENDOR)
            }
            R.id.bt_logintype_delivery -> {
                openLogin(Constant.LOGIN_TYPE_DELIVERY)
            }
        }
    }

    private fun openLogin(loginType: Int) {
        startActivity(
            Intent(ctx, LoginActivity::class.java)
                .putExtra("type", loginType)
        )
    }

    // check login
    private fun checkLogin() {
        if (sessionManager.isLoggedIn) {
            if (sessionManager.getIntData(SessionManager.KEY_TYPE) == Constant.LOGIN_TYPE_VENDOR) {
                startActivity(Intent(ctx, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(ctx, DeliveryMainActivity::class.java))
                finish()
            }
        }
    }
}
