package shree.techno.dabbaboyvendor.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import info.tech.dabbaboy.util.Constant
import kotlinx.android.synthetic.main.activity_splash.*
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.R

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startProgress()
        Handler().postDelayed(Runnable {
            startActivity(Intent(this, LoginTypeActivity::class.java))
            finish()
        }, Constant.SPLASH_TIMEOUT)
    }

    private fun startProgress() {
        var progressStatus = 0;
        Thread(Runnable {
            run {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    Thread.sleep(50);
                    pb_splash_progressBar.setProgress(progressStatus)
                }
            }
        }).start()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }
}
