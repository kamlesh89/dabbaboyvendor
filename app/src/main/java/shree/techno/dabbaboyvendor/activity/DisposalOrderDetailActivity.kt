package shree.techno.dabbaboyvendor.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_order_detail.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.adapter.MySpinnerAdapter
import shree.techno.dabbaboyvendor.pojo.DeliveryBoy
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class DisposalOrderDetailActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var obj: JSONObject
    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager
    var status: String? = null
    var deliv_boy_list: ArrayList<DeliveryBoy>? = null

    // api call type
    val CALL_API_TYPE_LOAD_DATA = 100
    val CALL_API_TYPE_READY = 101
    val CALL_API_TYPE_CANCEL = 102

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        iv_orderdetail_back.setOnClickListener(this)
        bt_orderdetail_pack.setOnClickListener(this)
        bt_orderdetail_cancel.setOnClickListener(this)
        obj = JSONObject(intent.getStringExtra("data"))
        setData()
        if (ConnectionDetector.isConnected()) {
            var body = ApiObjects.getTodaysOrderMenu(
                sessionManager.getData(SessionManager.KEY_ID),
                obj.getString("type"),
                obj.getString("date"),
                obj.getString("order_id"),
                obj.getString("pack_id")
            )
            loadData(CALL_API_TYPE_LOAD_DATA, WebApis.API_TODAY_ORDER_MENU, body)
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator)
        }
    }

    private fun setData() {
        tv_orderdetail_orderno.setText("Order No. " + obj.getString("id"))
        tv_orderdetail_packname.setText("( ${obj.getString("pack_name")} )")
        tv_orderdetail_tiffintype.setText(obj.getString("tiffin_type"))

        var qty = obj.getString("qty")
        var time = obj.getString("time")

        tv_orderdetail_qty.setText("Quantity : " + qty)
        if (!time.equals("")) {
            tv_orderdetail_delivtime.setText("Delivery Time : " + time)
        } else {
            tv_orderdetail_delivtime.setText("Delivery Time : Not Available")
        }
        var userobj = obj.getJSONObject("user")
        var addobj = obj.getJSONObject("address")
        tv_orderdetail_username.setText(userobj.getString("name"))
        tv_orderdetail_usermobile.setText(userobj.getString("mobile_no"))
        tv_orderdetail_deliveradd.setText(Utility.getAddress(addobj))

        status = obj.getString("status")
        if (status!!.equals("0")) {
            tv_orderdetail_spinnertitle.setText("Select Tiffin Code")
        } else {
            tv_orderdetail_spinnertitle.setText("Select Delivery Boy")
            bt_orderdetail_cancel.visibility = View.GONE
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.iv_orderdetail_back -> {
                finish()
            }
            R.id.bt_orderdetail_pack -> {
                if (isValids()) {
                    if (status.equals("0")) {
                        var body = ApiObjects.getdisposalReadyupadte(
                            obj.getString("id"),
                            obj.getString("type"),
                            sp_orderdetail_tiifincode.selectedItem.toString(),
                            sessionManager.getData(SessionManager.KEY_ID)
                        )
                        loadData(CALL_API_TYPE_READY, WebApis.API_DISPOSAL_TIFFIN_READY, body)
                    } else {
                        var body = ApiObjects.getAssigndisposalDeliveyboy(
                            obj.getString("id"),
                            obj.getString("type"),
                            deliv_boy_list!![sp_orderdetail_tiifincode.selectedItemPosition].id,
                            obj.getString("tiffin_code")
                        )
                        loadData(CALL_API_TYPE_READY, WebApis.API_DISPOSAL_TIFFIN_READY, body)
                    }
                }
            }
            R.id.bt_orderdetail_cancel -> {

            }
        }
    }

    private fun isValids(): Boolean {
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator)
            return false
        } else if (sp_orderdetail_tiifincode.selectedItemPosition == 0) {
            if (status.equals("0")) {
                Utility.snacbarShow("Select Tiffin Code", coordinator)
            } else {
                Utility.snacbarShow("Select Delivery Boy", coordinator)
            }
            return false
        }
        return true
    }

    // load menu data and tiffin data
    private fun loadData(type: Int, api: String, myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(api)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response, type)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject, type: Int) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            if (type == CALL_API_TYPE_LOAD_DATA) {
                setLoadData(response)
            } else if (type == CALL_API_TYPE_READY) {
                setResult(Activity.RESULT_OK, Intent())
                finish()
            }
        } else {
            Utility.snacbarShow(message, coordinator)
        }
    }

    private fun setLoadData(response: JSONObject) {
        var object_data = response.getJSONObject("data")
        var array_fix = object_data.getJSONArray("package")
        var array_selected = object_data.getJSONArray("Menu")
        var array_deliv_boy = object_data.getJSONArray("delivery_boy")
        tv_orderdetail_selectmenu.setText(JsonParser.getslectedItems(array_fix, array_selected))

        var tiffin_list = JsonParser.getDisposaltiffineCode()
        deliv_boy_list = JsonParser.getDeliveryboys(array_deliv_boy)

        if (status.equals("0")) {
            sp_orderdetail_tiifincode.adapter = ArrayAdapter<String>(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                tiffin_list
            )
        } else {
            val var_adapter = MySpinnerAdapter(
                this,
                R.layout.adp_spinnerview,
                R.id.tv_adpitem_name,
                deliv_boy_list
            )
            sp_orderdetail_tiifincode.adapter = var_adapter
        }
    }

}
