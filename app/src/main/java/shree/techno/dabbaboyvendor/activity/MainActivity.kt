package shree.techno.dabbaboyvendor.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import info.tech.dabbaboy.util.Utility
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_navigation.*
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.fragment.*
import shree.techno.dabbaboyvendor.util.SessionManager

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var ctx: Context
    var sessionManager: SessionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        ll_nav_dashboard.setOnClickListener(this)
        ll_nav_myorder.setOnClickListener(this)
        ll_nav_orderhist.setOnClickListener(this)
        ll_nav_menu.setOnClickListener(this)
        ll_nav_tiffin.setOnClickListener(this)
        ll_nav_help.setOnClickListener(this)
        ll_nav_logout.setOnClickListener(this)

        tv_nav_name.setText(sessionManager?.getData(SessionManager.KEY_NAME))
        tv_nav_mobile.setText(sessionManager?.getData(SessionManager.KEY_MOBILE))

        iv_main_menu.setOnClickListener(View.OnClickListener {
            opencloseDrawer()
        })

        // navigation click
        ll_nav_dashboard.setOnClickListener(this)
        ll_nav_myorder.setOnClickListener(this)
        ll_nav_orderhist.setOnClickListener(this)
        ll_nav_menu.setOnClickListener(this)
        ll_nav_tiffin.setOnClickListener(this)
        ll_nav_help.setOnClickListener(this)
        ll_nav_logout.setOnClickListener(this)
        setHome()
    }

    private fun opencloseDrawer() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            drawer.openDrawer(GravityCompat.START)
        }
    }

    override fun onClick(p0: View?) {
        opencloseDrawer()

        when (p0?.id) {
            R.id.ll_nav_dashboard -> {
                setHome()
            }
            R.id.ll_nav_myorder -> {
                Utility.setFragment(
                    FragmentMyOrder(ctx, ctx.getString(R.string.title_myorder)),
                    ctx.getString(R.string.title_myorder),
                    this
                )
            }
            R.id.ll_nav_orderhist -> {
                Utility.setFragment(
                    FragmentOrderHistory(ctx, ctx.getString(R.string.title_orderhistory)),
                    ctx.getString(R.string.title_orderhistory),
                    this
                )
            }
            R.id.ll_nav_menu -> {
                Utility.setFragment(
                    FragmentMenu(ctx, ctx.getString(R.string.title_menu)),
                    ctx.getString(R.string.title_menu),
                    this
                )
            }
            R.id.ll_nav_tiffin -> {
                Utility.setFragment(
                    FragmentTiffin(ctx, ctx.getString(R.string.title_tiffin)),
                    ctx.getString(R.string.title_tiffin),
                    this
                )
            }
            R.id.ll_nav_help -> {
                Utility.setFragment(
                    FragmentHelpsupport(ctx, ctx.getString(R.string.title_help)),
                    ctx.getString(R.string.title_help),
                    this
                )
            }
            R.id.ll_nav_logout -> {
                userLogout()
            }
        }
    }

    private fun setHome() {
        Utility.setFragment(
            FragmentDashboard(ctx, ctx.getString(R.string.title_dashboard)),
            ctx.getString(R.string.title_dashboard),
            this
        )
    }


    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (tv_main_title.getText().toString() == ctx.getString(R.string.title_dashboard)) {
                closeAppDialaog()
            } else {
                setHome()
            }
        }
    }

    fun closeAppDialaog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(
            ctx.getString(R.string.exit_app)
        ).setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
                finishAffinity()
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    private fun userLogout() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(ctx.getString(R.string.logout_msg))
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
                SessionManager(ctx).logoutUser(this)
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    fun setHeadTitle(title: String) {
        tv_main_title.setText(title)
    }

    // on activity result
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }


}
