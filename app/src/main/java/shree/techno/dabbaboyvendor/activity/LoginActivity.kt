package shree.techno.dabbaboyvendor.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Constant
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.deliveryboy.DeliveryMainActivity
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class LoginActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    var type: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }


    private fun initXml() {
        ctx = this
        bt_login_signin.setOnClickListener(this)
        tv_login_forgot.setOnClickListener(this)

        type = intent.getIntExtra("type", 125)
        if (type!! == Constant.LOGIN_TYPE_VENDOR) {
            tv_login_welcomefor.setText("Vendor Log In!")
        } else {
            tv_login_welcomefor.setText("Delivery Log In!")
        }
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.bt_login_signin -> {
                if (isValid()) {
                    var body = ApiObjects.getLogin(
                        met_login_mobile.text.toString(),
                        met_login_pass.text.toString()
                    )
                    if (type!! == Constant.LOGIN_TYPE_VENDOR) {
                        userLogin(WebApis.API_LOGIN, body)
                    } else {
                        userLogin(WebApis.API_DEL_LOGIN, body)
                    }
                }
            }
            R.id.tv_login_forgot -> {
                startActivity(Intent(ctx, ForgotActivity::class.java))
            }
        }
    }

    private fun userLogin(api: String, myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(api)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            if (type!! == Constant.LOGIN_TYPE_VENDOR) {
                setVendorResponse(response)
            } else {
                startmyService()
                setDeliveryResponse(response)
            }
        } else {
            Utility.snacbarShow(message, coordinator_login)
        }
    }

    private fun startmyService() {
    }

    private fun setVendorResponse(response: JSONObject) {
        var data_obj = response.getJSONObject("data")
        SessionManager(this).createLoginSession(
            data_obj.getString("id"), data_obj.getString("name")
            , data_obj.getString("email"), data_obj.getString("mobile_no")
            , data_obj.getString("address"), data_obj.getString("pin_code")
            , type!!
        )
        finishAffinity()
        startActivity(Intent(ctx, MainActivity::class.java))
    }

    private fun setDeliveryResponse(response: JSONObject) {
        var data_obj = response.getJSONObject("data")
        SessionManager(this).createLoginSession(
            data_obj.getString("id"), data_obj.getString("dilivery_boy_name")
            , data_obj.getString("email_id"), data_obj.getString("mobile_no")
            , data_obj.getString("aadhar_no"), data_obj.getString("boy_image")
            , type!!
        )
        finishAffinity()
        startActivity(Intent(ctx, DeliveryMainActivity::class.java))
    }


    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_login)
            return false
        } else if (met_login_mobile.text.toString().length != 10) {
            met_login_mobile.setError("Enter 10 Digit Mobile Number")
            met_login_mobile.requestFocus()
            return false
        } else if (met_login_pass.text.toString().isEmpty()) {
            met_login_pass.setError("Enter Password")
            met_login_pass.requestFocus()
            return false
        }
        return valid
    }

}
