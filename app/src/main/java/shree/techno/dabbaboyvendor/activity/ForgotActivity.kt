package shree.techno.dabbaboyvendor.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_forgot.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.util.ConnectionDetector

class ForgotActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        bt_forgot_next.setOnClickListener(this)
        iv_header_back.setOnClickListener(this)

        tv_header_title.setText(ctx.getString(R.string.title_forgot))
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_forgot
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.bt_forgot_next -> {
                if (isValid()) {
                    usergetPassword(ApiObjects.getMode(met_forgot_mobile.text.toString()))
                }
            }
            R.id.iv_header_back -> {
                finish()
            }
        }
    }

    private fun usergetPassword(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_FORGOT)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        Utility.snacbarShow(message, coordinator_forgot)
        if (status == 200) {
            finish()
        }
    }


    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_forgot)
            return false
        } else if (met_forgot_mobile.text.toString().length != 10) {
            met_forgot_mobile.setError("Enter 10 Digit Mobile Number")
            met_forgot_mobile.requestFocus()
            return false
        }
        return valid
    }
}
