package shree.techno.dabbaboyvendor.activity

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.textfield.TextInputEditText
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.BaseActivity
import shree.techno.dabbaboyvendor.HeaderItemClickListener
import shree.techno.dabbaboyvendor.MenuItemClickListener
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.adapter.PackHeaderAdapter
import shree.techno.dabbaboyvendor.pojo.MenuItem
import shree.techno.dabbaboyvendor.pojo.PackHeader
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.MyCalendar
import shree.techno.dabbaboyvendor.util.SessionManager
import java.util.*
import kotlin.collections.ArrayList


class MenuActivity : BaseActivity(), View.OnClickListener, HeaderItemClickListener,
    MenuItemClickListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager
    lateinit var list: ArrayList<PackHeader>
    lateinit var fixitem_list: ArrayList<PackHeader>

    // constant
    val API_TYPE_LOAD = "load"
    val API_TYPE_DELETE = "delete"
    val API_TYPE_EDIT = "edit"
    val INTENT_PACK_ID = "pack_id"

    var spinner_pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_menu
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        tv_header_title.setText(intent.getStringExtra("name"))

        iv_header_back.setOnClickListener(this)

        // date spinner
        sp_menu_createfordate.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                MyCalendar.getnext15Days()
            )
        )
        sp_menu_createfordate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?,
                position: Int, id: Long
            ) {
                getData()
            }
        }

    }

    private fun getData() {
        if (ConnectionDetector.isConnected()) {
            callApi(
                ApiObjects.getpackHeaders(
                    intent.getStringExtra(INTENT_PACK_ID),
                    sessionManager.getData(SessionManager.KEY_ID),
                    sp_menu_createfordate.selectedItem.toString()
                ),
                API_TYPE_LOAD,
                WebApis.API_PACKAGE_HEADER
            )
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_menu)
        }
    }

    private fun callApi(apibody: JSONObject, type: String, api: String) {

        Utility.showLoader(ctx)
        AndroidNetworking.post(api)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response, type);
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject, type: String) {
        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            if (type.equals(API_TYPE_LOAD)) {
                var data_array = response.getJSONArray("data")
                list = JsonParser.getpackHeader(data_array)
                fixitem_list = JsonParser.getFixItems(data_array)
            } else {
                getData()
            }
        } else {
            if (type.equals(API_TYPE_LOAD)) {
                list = ArrayList()
                fixitem_list = ArrayList()
            }
            Utility.snacbarShow(message, coordinator_menu)
        }
        setSpinner()
    }

    private fun setAdapter() {

        var filter_fixlist = getFilterlist(fixitem_list)
        var items = JsonParser.getFixitemstring(filter_fixlist)
        if (items.equals("null")) {
            tv_menu_fixitem.setText("Not Available")
        } else {
            tv_menu_fixitem.setText(items)
        }

        if (list.size > 0) {
            rv_menu_recyclerview.setHasFixedSize(true)
            rv_menu_recyclerview.setLayoutManager(LinearLayoutManager(ctx))
            rv_menu_recyclerview.setAdapter(PackHeaderAdapter(ctx, getFilterlist(list), this, this))
        }


    }

    private fun getFilterlist(list: ArrayList<PackHeader>): java.util.ArrayList<PackHeader> {
        if (list.size > 0) {
            var type = sp_menu_createfor.selectedItem.toString()
            var filterlist = ArrayList<PackHeader>()
            for (i in 0..list.size - 1) {
                if (type.equals(list.get(i).lunchdinner)) {
                    filterlist.add(list[i])
                }
            }
            return filterlist
        } else {
            return list
        }
    }

    private fun setSpinner() {

        // type spinner
        sp_menu_createfor.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(*resources.getStringArray(R.array.select_menu_type))
            )
        )
        sp_menu_createfor.setSelection(spinner_pos)
        sp_menu_createfor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?,
                position: Int, id: Long
            ) {
                spinner_pos = position
                if (fixitem_list.size > 0) {
                    setAdapter()
                }
            }
        }

    }


    override fun onClick(p0: View?) {

        when (p0?.id) {

            R.id.iv_header_back -> {
                finish()
            }
        }
    }

    private fun menuDialog(
        button_text: String,
        menuItem: MenuItem
    ) {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_addmenu)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.window!!.attributes)
        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.4f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        dialog.window!!.attributes = layoutParams

        val add_bt: Button = dialog.findViewById(R.id.bt_dialogmenu_add) as Button
        val itemname_met: TextInputEditText = dialog.findViewById(R.id.met_dialogmenu_itemname)
        add_bt.setText(button_text)
        itemname_met.setText(menuItem.item_name)

        add_bt.setOnClickListener(View.OnClickListener {
            var name = itemname_met.text.toString()
            if (name.isEmpty()) {
                itemname_met.setError("Enter Item Name")
            } else if (!ConnectionDetector.isConnected()) {
                Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_menu)
            } else {
                menuItem.item_name = name
                addItem(dialog, menuItem, add_bt.text.toString())
            }
        })
        dialog.show()
    }

    private fun addItem(dialog: Dialog, menuItem: MenuItem, type: String) {

        var apibody = getApiBody(menuItem, type);
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ADD_MENU)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    var info_obj = response.getJSONObject("info")
                    var status = info_obj.getInt("status")
                    var message = info_obj.getString("message")
                    Utility.snacbarShow(message, coordinator_menu)
                    if (status == 200) {
                        dialog.dismiss()
                        getData()
                    }
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun getApiBody(menuItem: MenuItem, type: String): JSONObject {
        var vendor_id = sessionManager.getData(SessionManager.KEY_ID)
        if (type.equals(ctx.getString(R.string.add_bt))) {
            return ApiObjects.addMenuItem(
                menuItem, vendor_id,
                sp_menu_createfordate.selectedItem.toString()
            )
        } else {
            return ApiObjects.editMenuItem(menuItem, sp_menu_createfordate.selectedItem.toString())
        }
    }

    override fun clickonAdd(packheader: PackHeader) {
        menuDialog(
            ctx.getString(R.string.add_bt),
            MenuItem(
                "", "", "", "",
                packheader.pack_id, packheader.id, packheader.lunchdinner,""
            )
        )
    }

    override fun menuItemClickOnEdit(menuItem: MenuItem) {
        menuDialog(ctx.getString(R.string.update_bt), menuItem)
    }

    override fun menuItemClickOnDelete(menuItem: MenuItem) {
        if (ConnectionDetector.isConnected()) {
            callApi(
                ApiObjects.deleteMenuItem(menuItem.item_id),
                API_TYPE_DELETE, WebApis.API_ADD_MENU
            )
        }
    }
}
