package shree.techno.dabbaboyvendor.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.adapter.DashboardAdapter
import shree.techno.dabbaboyvendor.pojo.Dashboard
import shree.techno.dabbaboyvendor.pojo.TodaysOrders
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class FragmentDashboard(var ctx: Context, var title: String) : Fragment() {

    var recyclerView: RecyclerView? = null
    var coordinatorLayout: CoordinatorLayout? = null
    var type_sp: Spinner? = null
    var sessionManager: SessionManager? = null

    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_dashboard, container, false)
        initXml(myview)
        return myview
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    private fun initXml(myview: View) {

        recyclerView = myview.findViewById(R.id.rv_dashboard_recyclerview)
        coordinatorLayout = myview.findViewById(R.id.coordinator_dashboard)
        type_sp = myview.findViewById(R.id.sp_dashboard_type)

        setSpinner()
    }

    private fun setSpinner() {
        var list = ArrayList<String>()
        list.add(TYPE_LUNCH)
        list.add(TYPE_DINNER)
        type_sp?.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list)

        type_sp?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (ConnectionDetector.isConnected()) {
                    loadData(
                        ApiObjects.getTodaysOrder(
                            sessionManager!!.getData(SessionManager.KEY_ID),
                            type_sp?.selectedItem.toString(),
                            Utility.getDate()
                        )
                    )
                } else {
                    Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    private fun loadData(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_TODAY_ORDER)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            recyclerView?.visibility = View.VISIBLE
            var obj_data = response.getJSONObject("data")
            var all_order_array = obj_data.getJSONArray("all_order")
            var free_order_array = obj_data.getJSONArray("free_tiffin")

            if (all_order_array.length() > 0 || free_order_array.length() > 0) {
                if (type_sp!!.selectedItem.toString().equals(TYPE_LUNCH)) {
                    var list = JsonParser.getDashboardlist(all_order_array, TYPE_LUNCH)
                    var order_list = JsonParser.getTodayLunchlist(all_order_array)
                    setAdapter(list, order_list)
                } else {
                    var list = JsonParser.getDashboardlist(all_order_array, TYPE_DINNER)
                    var order_list = JsonParser.getTodayDinnerlist(all_order_array)
                    setAdapter(list, order_list)
                }
            } else {
                recyclerView?.visibility = View.GONE
            }
        } else {
            recyclerView?.visibility = View.GONE
            Utility.snacbarShow(message, coordinatorLayout!!)
        }
    }

    private fun setAdapter(
        list: ArrayList<Dashboard>,
        orderList: ArrayList<TodaysOrders>
    ) {

        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(ctx)
        recyclerView?.adapter = DashboardAdapter(ctx, list, orderList)
    }

}
