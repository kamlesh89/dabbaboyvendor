package shree.techno.dabbaboyvendor.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.adapter.MyFreeOrderAdapter
import shree.techno.dabbaboyvendor.adapter.MySubsOrderAdapter
import shree.techno.dabbaboyvendor.pojo.FreeOrder
import shree.techno.dabbaboyvendor.pojo.TodaysOrders
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager
import kotlin.collections.ArrayList

class FragmentMyOrder(var ctx: Context, var title: String) : Fragment() {

    var recyclerView: RecyclerView? = null
    var coordinatorLayout: CoordinatorLayout? = null
    var type_sp: Spinner? = null
    var freesubs_sp: Spinner? = null
    var sessionManager: SessionManager? = null

    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    val TYPE_SUBSCRIBE = "Subscribe"
    val TYPE_FREE = "Free"

    val INTENT_ORDER_DETAIL = 101

    var order_list: ArrayList<TodaysOrders>? = null
    var free_list: ArrayList<FreeOrder>? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_myorders, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View) {

        recyclerView = myview?.findViewById(R.id.rv_myorder_recyclerview)
        coordinatorLayout = myview?.findViewById(R.id.coordinator_myorder)
        type_sp = myview?.findViewById(R.id.sp_myorder_type)
        freesubs_sp = myview?.findViewById(R.id.sp_myorder_freesubs)

        setSpinner()
    }

    private fun setSpinner() {
        // type
        var list = ArrayList<String>()
        list.add(TYPE_LUNCH)
        list.add(TYPE_DINNER)
        type_sp?.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list)
        type_sp?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (ConnectionDetector.isConnected()) {
                    order_list = ArrayList()
                    free_list = ArrayList()
                    loadData(
                        ApiObjects.getTodaysOrder(
                            sessionManager!!.getData(SessionManager.KEY_ID),
                            type_sp?.selectedItem.toString(),
                            Utility.getDate()
                        )
                    )
                } else {
                    Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    private fun loadData(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_TODAY_ORDER)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            recyclerView?.visibility = View.VISIBLE
            var obj_data = response.getJSONObject("data")
            var all_order_array = obj_data.getJSONArray("all_order")
            var free_order_array = obj_data.getJSONArray("free_tiffin")
            free_list =
                JsonParser.getTodayfreeOrder(free_order_array, type_sp?.selectedItem.toString())

            if (type_sp?.selectedItem.toString().equals(TYPE_LUNCH)) {
                order_list = JsonParser.getTodayLunchlist(all_order_array)
            } else {
                order_list = JsonParser.getTodayDinnerlist(all_order_array)
            }
            setsubsfreeSpinner()
        } else {
            recyclerView?.visibility = View.GONE
            Utility.snacbarShow(message, coordinatorLayout!!)
        }
    }

    fun setsubsfreeSpinner() {
        // free
        var free_sp_list = ArrayList<String>()
        free_sp_list.add(TYPE_SUBSCRIBE)
        free_sp_list.add(TYPE_FREE)
        freesubs_sp?.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, free_sp_list)

        freesubs_sp?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (freesubs_sp?.selectedItem.toString().equals(TYPE_SUBSCRIBE)) {
                    setOrderAdapter(order_list!!)
                } else {
                    setfreeOrderAdapter(free_list!!)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    private fun setOrderAdapter(list: ArrayList<TodaysOrders>) {

        if (list.size > 0) {
            recyclerView?.visibility = View.VISIBLE
            recyclerView?.setHasFixedSize(true)
            recyclerView?.layoutManager = LinearLayoutManager(ctx)
            recyclerView?.adapter =
                MySubsOrderAdapter(ctx, list, type_sp?.selectedItem.toString())
        } else {
            recyclerView?.visibility = View.GONE
        }
    }

    private fun setfreeOrderAdapter(freelist: ArrayList<FreeOrder>) {

        if (freelist.size > 0) {
            recyclerView?.visibility = View.VISIBLE
            recyclerView?.setHasFixedSize(true)
            recyclerView?.layoutManager = LinearLayoutManager(ctx)
            recyclerView?.adapter =
                MyFreeOrderAdapter(ctx, freelist, type_sp?.selectedItem.toString())
        } else {
            recyclerView?.visibility = View.GONE
        }
    }

    //on activity result
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == INTENT_ORDER_DETAIL && resultCode == Activity.RESULT_OK) {
//            initXml(myview)
            Utility.setFragment(
                FragmentMyOrder(ctx, ctx.getString(R.string.title_myorder)),
                ctx.getString(R.string.title_myorder),
                ctx as MainActivity
            )
        }
    }

}
