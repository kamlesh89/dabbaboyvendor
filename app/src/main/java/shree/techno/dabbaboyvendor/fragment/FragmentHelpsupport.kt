package shree.techno.dabbaboyvendor.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity

class FragmentHelpsupport(var ctx: Context, var title: String) : Fragment() {

    init {
        (ctx as MainActivity).setHeadTitle(title)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_help, container, false)
        return root
    }
}
