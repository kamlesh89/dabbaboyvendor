package shree.techno.dabbaboyvendor.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.layout_recyclerview.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.adapter.TiffinAdapter
import shree.techno.dabbaboyvendor.pojo.Tiffin
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class FragmentTiffin(var ctx: Context, var title: String) : Fragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout
    var sessionManager: SessionManager

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.layout_recyclerview, container, false)
        initXml(myview)
        if (ConnectionDetector.isConnected()) {
            laodData(ApiObjects.getTiifinBody(sessionManager.getData(SessionManager.KEY_ID)))
        }
        return myview
    }

    private fun initXml(myview: View?) {
        recyclerView = myview!!.findViewById(R.id.layout_recyclerview)
        coordinatorLayout = myview!!.findViewById(R.id.coordinator_recyclerview)
    }

    private fun laodData(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_TIFFIN)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var array_data = response.getJSONArray("data")
            var object_data = array_data.getJSONObject(0)
            var tiffin_data = object_data.getJSONArray("user_dataa")
            var list = JsonParser.getAllTiffinlist(tiffin_data)
            setAdapter(list)
        } else {
            Utility.snacbarShow(message, coordinator_recyclerview)
        }
    }


    private fun setAdapter(list: ArrayList<Tiffin>) {
        if (list!!.size > 0) {
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.adapter = TiffinAdapter(list!!)
        }
    }
}
