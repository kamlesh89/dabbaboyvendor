package shree.techno.dabbaboyvendor.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.adapter.MyMenuAdapter
import shree.techno.dabbaboyvendor.pojo.MealPackage
import shree.techno.dabbaboyvendor.util.ConnectionDetector

class FragmentMenu(var ctx: Context, var title: String) : Fragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout

    init {
        (ctx as MainActivity).setHeadTitle(title)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.layout_recyclerview, container, false)
        initXml(root)
        if (ConnectionDetector.isConnected()) {
            laodData(ApiObjects.getMode("0"))
        }
        return root
    }

    private fun initXml(root: View) {
        recyclerView = root.findViewById(R.id.layout_recyclerview)
        coordinatorLayout = root.findViewById(R.id.coordinator_recyclerview)
    }

    private fun laodData(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_PACKAGE)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var array_data = response.getJSONArray("data")
            var list = JsonParser.getpackagelist(array_data)
            setAdapter(list)
        } else {
            Utility.snacbarShow(message, coordinator_content)
        }
    }

    private fun setAdapter(list: ArrayList<MealPackage>) {
        recyclerView.setHasFixedSize(true)
        recyclerView.setLayoutManager(LinearLayoutManager(ctx))
        recyclerView.setAdapter(MyMenuAdapter(ctx, list))
    }

}
