package shree.techno.dabbaboyvendor.fragment

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import kotlinx.android.synthetic.main.layout_recyclerview.*
import org.json.JSONObject
import shree.techno.dabbaboyvendor.R
import shree.techno.dabbaboyvendor.activity.MainActivity
import shree.techno.dabbaboyvendor.adapter.OrderHistoryAdapter
import shree.techno.dabbaboyvendor.adapter.TiffinAdapter
import shree.techno.dabbaboyvendor.pojo.OrderHistory
import shree.techno.dabbaboyvendor.pojo.Tiffin
import shree.techno.dabbaboyvendor.util.ConnectionDetector
import shree.techno.dabbaboyvendor.util.SessionManager

class FragmentOrderHistory(var ctx: Context, var title: String) : Fragment(), View.OnClickListener {

    lateinit var recyclerView: RecyclerView
    var lunch_tv: TextView? = null
    var dinner_tv: TextView? = null
    lateinit var coordinatorLayout: CoordinatorLayout
    var sessionManager: SessionManager

    var lunch_list: ArrayList<OrderHistory>? = null
    var dinner_list: ArrayList<OrderHistory>? = null

    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.layout_recyclerview, container, false)
        initXml(myview)
        if (ConnectionDetector.isConnected()) {
            laodData(ApiObjects.getTiifinBody(sessionManager.getData(SessionManager.KEY_ID)))
        }
        return myview
    }

    private fun initXml(myview: View?) {
        recyclerView = myview!!.findViewById(R.id.layout_recyclerview)
        coordinatorLayout = myview!!.findViewById(R.id.coordinator_recyclerview)
        lunch_tv = myview!!.findViewById(R.id.tv_recycler_lunch)
        dinner_tv = myview!!.findViewById(R.id.tv_recycler_dinner)

        lunch_tv?.visibility = View.VISIBLE
        dinner_tv?.visibility = View.VISIBLE

        lunch_tv?.setOnClickListener(this)
        dinner_tv?.setOnClickListener(this)
    }

    private fun laodData(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ORDER_HISTORY)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var obj_data = response.getJSONObject("data")
            var lunch_data = obj_data.getJSONArray("Lunch")
            var dinner_data = obj_data.getJSONArray("Dinner")
            lunch_list = JsonParser.getlunchOrderHistory(lunch_data)
            dinner_list = JsonParser.getdinnerOrderHistory(dinner_data)
            setAdapter(lunch_list!!)
        } else {
            Utility.snacbarShow(message, coordinator_recyclerview)
        }
    }


    private fun setAdapter(list: ArrayList<OrderHistory>) {
        if (list!!.size > 0) {
            recyclerView.visibility = View.VISIBLE
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.adapter = OrderHistoryAdapter(list!!)
        }else{
            recyclerView.visibility = View.GONE
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.tv_recycler_lunch -> {
                setAdapter(lunch_list!!)
                setIndicate(TYPE_LUNCH)
            }
            R.id.tv_recycler_dinner -> {
                setAdapter(dinner_list!!)
                setIndicate(TYPE_DINNER)
            }
        }
    }

    private fun setIndicate(type: String) {
        if (type.equals(TYPE_LUNCH)) {
            lunch_tv?.setTypeface(null, Typeface.BOLD)
            dinner_tv?.setTypeface(null, Typeface.NORMAL)
        } else {
            dinner_tv?.setTypeface(null, Typeface.BOLD)
            lunch_tv?.setTypeface(null, Typeface.NORMAL)
        }
    }

}
